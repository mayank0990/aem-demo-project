package com.aem.demo.listeners;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.observation.Event;
import javax.jcr.observation.EventIterator;
import javax.jcr.observation.EventListener;

import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.jcr.api.SlingRepository;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.demo.constants.DemoConstants;

/**
 * Event Listener for Replication Events
 * 
 * @author Mayank
 *
 */
@Component(immediate = true, service = EventListener.class)
public class SampleEventListener implements EventListener {

	private final Logger LOGGER = LoggerFactory.getLogger(SampleEventListener.class);

	@Reference
	private ResourceResolverFactory resourceResolverFactory;
	
	@Reference
	private SlingRepository repository;
	
	private static String[] TYPES = {"dam:Asset"};	
	private static final String EVENT_LISTEN_PATH = "/content/dam/aem-demo";

	private Session session;
	
	@Activate
	public void activate(ComponentContext context) {
		LOGGER.info("Activated SampleEventListener");
		try {
			session = repository.loginService(DemoConstants.DEMO_WRITE_SUBSERVICE, null);
			if(session != null) {
				session.getWorkspace().getObservationManager().addEventListener(
						this, 
						Event.NODE_ADDED,
						EVENT_LISTEN_PATH, 
						false, 
						null,  
						null, 
						false);
			}
		} catch (RepositoryException e) {
			LOGGER.error("unable to register session",e);
		}
	}
	
	@Deactivate
	public void deactivate() throws RepositoryException {
		if (session != null) {
			session.logout();
		}
	}

	@Override
	public void onEvent(EventIterator eventIterator) {
		try {
			while (eventIterator.hasNext()){
				LOGGER.info("Something has been added : {}", eventIterator.nextEvent().getPath());
			}
		} catch(RepositoryException e){
			LOGGER.error("Error while treating events",e);
		}
	}
}