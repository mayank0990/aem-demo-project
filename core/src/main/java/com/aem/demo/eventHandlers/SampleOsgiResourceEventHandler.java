package com.aem.demo.eventHandlers;

import org.apache.sling.api.SlingConstants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is Resource Event Handler which basically tells any resource is added, changed, moved or deleted
 * 
 * @author Mayank
 *
 */
@Component(immediate = true, service = EventHandler.class, property = {
		EventConstants.EVENT_FILTER + "=" + "(path=/content/*)",
		EventConstants.EVENT_TOPIC + "=org/apache/sling/api/resource/Resource/*" })
public class SampleOsgiResourceEventHandler implements EventHandler {

	private final Logger LOGGER = LoggerFactory.getLogger(getClass());

	public void handleEvent(final Event event) {
		LOGGER.info("OSGi EventHandler: {} at: {}", event.getTopic(), event.getProperty(SlingConstants.PROPERTY_PATH));
	}
}