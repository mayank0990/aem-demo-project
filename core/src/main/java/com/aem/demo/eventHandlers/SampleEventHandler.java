package com.aem.demo.eventHandlers;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.replication.ReplicationAction;

/**
 * Event Listener for Replication Events
 * 
 * @author Mayank
 *
 */
@Component(immediate = true, service = EventHandler.class, property = {
		EventConstants.EVENT_TOPIC + "=" + ReplicationAction.EVENT_TOPIC })
public class SampleEventHandler implements EventHandler {

	private final Logger LOGGER = LoggerFactory.getLogger(SampleEventHandler.class);

	@Override
	public void handleEvent(Event event) {
		ReplicationAction action = ReplicationAction.fromEvent(event);		 
		if(action != null) {
			LOGGER.info("Replication action {} occured on {} ", action.getType().getName(), action.getPath());
		}
	}	
}
