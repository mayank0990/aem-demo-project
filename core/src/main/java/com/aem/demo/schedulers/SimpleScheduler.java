package com.aem.demo.schedulers;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.demo.config.SimpleSchedulerConfig;

/**
 * A simple demo for cron-job like tasks that get executed regularly.
 * It also demonstrates how property values can be set. Users can
 * set the property values in /system/console/configMgr
 */
@Designate(ocd = SimpleSchedulerConfig.class)
@Component(service = Runnable.class)
public class SimpleScheduler implements Runnable {
	
	 private final Logger LOGGER = LoggerFactory.getLogger(getClass());
	 
	 private String myParameter;
	 
	 @Activate
	 public void activate(final SimpleSchedulerConfig config) {
		 myParameter = config.myParameter();
	 }
	 
	 @Modified
	 protected void modify(final SimpleSchedulerConfig config) {
		 synchronized(this.myParameter) {
			 
		 }
	 }
	 
	 @Override
	 public void run() {
		 LOGGER.info("SimpleScheduler is now running, myParameter : {} ", myParameter);
	 }
}
