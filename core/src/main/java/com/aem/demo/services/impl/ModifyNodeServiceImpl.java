package com.aem.demo.services.impl;

import javax.jcr.Node;
import javax.jcr.Session;

import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.demo.constants.DemoConstants;
import com.aem.demo.services.ModifyNodeService;
import com.aem.demo.utils.DemoUtil;

@Component(service = ModifyNodeService.class, immediate = true, property = {
		Constants.SERVICE_DESCRIPTION + "= Modify Node Service Implmentation"
})
public class ModifyNodeServiceImpl implements ModifyNodeService {
	
	/** Default Logger */
	protected final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
	
	@Reference
	private ResourceResolverFactory resourceResolverFactory;
	
	@Activate
	protected void activate() {
		LOGGER.info("ModifyNodeServiceImpl Activated!");
	}
	
	@Override
	public void updateNode(String path) {
		try {
			final ResourceResolver resolver = DemoUtil.getResourceResolver(resourceResolverFactory, DemoConstants.DEMO_WRITE_SUBSERVICE);
			final Session session = resolver.adaptTo(Session.class);	
			LOGGER.info("UserID for ModifyNodeServiceImpl :: " + resolver.getUserID());
			Node root = session.getRootNode();
			String finalPath = path.replaceFirst("/", "");
			Node contentNode = root.getNode(finalPath);
			contentNode.setProperty("name", "Mayank");
			session.save();
			session.logout();			
		} catch(Exception e) {
			LOGGER.info("Error in updateNode() method of ModifyNodeServiceImpl :: {} ", e);
			e.printStackTrace();
		}
	}
}
