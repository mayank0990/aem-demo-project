package com.aem.demo.services;

import javax.jcr.RepositoryException;

import org.osgi.annotation.versioning.ConsumerType;

@ConsumerType
public interface UMSService {
	
	public void getUser() throws RepositoryException;

}
