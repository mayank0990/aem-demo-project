package com.aem.demo.services;

import java.util.List;

import org.apache.sling.api.resource.Resource;
import org.osgi.annotation.versioning.ConsumerType;

@ConsumerType
public interface NodeIteratorService {
	
	public List<Resource> getResourceList();

}
