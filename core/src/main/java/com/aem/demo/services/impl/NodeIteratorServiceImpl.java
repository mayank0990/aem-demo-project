package com.aem.demo.services.impl;

import static org.apache.sling.query.SlingQuery.$;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.query.SlingQuery;
import org.apache.sling.query.api.Predicate;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.demo.config.NodeServiceConfig;
import com.aem.demo.constants.DemoConstants;
import com.aem.demo.services.NodeIteratorService;
import com.aem.demo.utils.DemoUtil;

@Component(immediate = true, service = { NodeIteratorService.class }, enabled = true,
	configurationPolicy = ConfigurationPolicy.REQUIRE,
	property = {
			Constants.SERVICE_DESCRIPTION + "= Node Iterator Service Implementation" })
@Designate(ocd = NodeServiceConfig.class)
public class NodeIteratorServiceImpl implements NodeIteratorService {

	private static Logger LOGGER = LoggerFactory.getLogger(NodeIteratorServiceImpl.class);
	
	@Reference
	private ResourceResolverFactory resourceResolverFactory;

	private ResourceResolver resourceResolver;

	private String parentPagePath = StringUtils.EMPTY;
	private String nodeType = StringUtils.EMPTY;
	private String nodeProperty = StringUtils.EMPTY;

	@Activate
	public void activate(NodeServiceConfig config) {
		LOGGER.info("NodeIteratorService Activated!");
		resourceResolver = DemoUtil.getResourceResolver(resourceResolverFactory, DemoConstants.DEMO_READ_SUBSERVICE);		

		parentPagePath = config.parentPagePath();
		nodeType = config.type();
		nodeProperty = config.property();
		
		LOGGER.info("Parent Path: " + parentPagePath);
		LOGGER.info("Node Type: " + nodeType);
		LOGGER.info("Node Property: " + nodeProperty);
	}

	/**
	 * Returns {@link List<Resource>} for the given parent path, 
	 * 
	 */
	@Override
	public List<Resource> getResourceList() {
		List<Resource> list = new ArrayList<>();
		try {
			Resource resource = resourceResolver.getResource(parentPagePath);		
			SlingQuery resources = $(resource).find(nodeType).filter(new Predicate<Resource>() {
				@Override
				public boolean accepts(Resource element) {
					return !element.getValueMap().containsKey(nodeProperty);
				}	
			});
			
			for(Resource res : resources) {
				list.add(res);
			}
		} catch(Exception e) {
			LOGGER.error("Error in NodeIteratorService : getResourceList() : ", e);
		}
		return list;
	}
}
