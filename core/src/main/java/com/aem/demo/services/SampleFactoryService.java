package com.aem.demo.services;

import org.osgi.annotation.versioning.ConsumerType;

@ConsumerType
public interface SampleFactoryService {
	
	public String getName();

}
