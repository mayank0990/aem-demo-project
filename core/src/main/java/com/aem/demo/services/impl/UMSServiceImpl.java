package com.aem.demo.services.impl;

import java.security.Principal;
import java.util.Iterator;

import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.Query;
import org.apache.jackrabbit.api.security.user.QueryBuilder;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.demo.constants.DemoConstants;
import com.aem.demo.services.UMSService;
import com.aem.demo.utils.DemoUtil;

@Component( immediate = true, service = { UMSService.class }, enabled = true,
		property = {
				Constants.SERVICE_DESCRIPTION + "= UMS Sample Service" })
public class UMSServiceImpl implements UMSService {
	
	/**Default Logger*/
	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
	
	@Reference
	ResourceResolverFactory resourceResolverFactory;
	
	ResourceResolver resourceResolver;
	
	Session session;
	
	@Activate
	protected void activate() {
		LOGGER.info("UMSServiceImpl Activated!");
	}
	
	@Override
	public void getUser() throws RepositoryException {
		resourceResolver = DemoUtil.getResourceResolver(resourceResolverFactory, DemoConstants.DEMO_READ_SUBSERVICE);
		session = resourceResolver.adaptTo(Session.class);
		LOGGER.info("UserId for UMSServiceImpl :: " + session.getUserID());
		
		//ValueFactory vf = session.getValueFactory();
		final UserManager userMgr = resourceResolver.adaptTo(UserManager.class);				
		LOGGER.info("Node Path via AuthorizableId :: " + userMgr.getAuthorizable("akash").getPath());
		LOGGER.info("Node Path via PrincipalName :: " + userMgr.getAuthorizable(new SimplePrincipal("akash-maggon")).getPath());
		
		try {
			/*if(null != authUser) {*/
				Iterator<Authorizable> result = userMgr.findAuthorizables(new Query() {
				    public <T> void build(QueryBuilder<T> builder) {
				    	 builder.setCondition(builder.contains("profile/@randomProp", "wow"));
				    }
				});
				while(result.hasNext()) {
					LOGGER.info("ID :: " + result.next().getID());
				}
			/*} else {
				LOGGER.info("AuthUser is Null");
			}*/
		} catch(Exception e) {
			LOGGER.error("Error in getUser() method :: {} ", e.getMessage());
		}
	}
	
	private static class SimplePrincipal implements Principal {
        protected final String name;
 
        public SimplePrincipal(String name) {
            if (name == null || name.trim().length() == 0) {
                throw new IllegalArgumentException("Principal name cannot be blank.");
            }
            this.name = name;
        }
 
        public String getName() {
            return name;
        }
 
        @Override
        public int hashCode() {
            return name.hashCode();
        }
 
        @Override
        public boolean equals(Object obj) {
            if (obj instanceof Principal) {
                return name.equals(((Principal) obj).getName());
            }
            return false;
        }
    }
}
