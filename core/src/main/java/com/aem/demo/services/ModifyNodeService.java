package com.aem.demo.services;

import org.osgi.annotation.versioning.ConsumerType;

@ConsumerType
public interface ModifyNodeService {
	public void updateNode(String paht);
}
