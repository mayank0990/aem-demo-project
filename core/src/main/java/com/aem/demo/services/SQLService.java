package com.aem.demo.services;

import java.sql.SQLException;

import org.osgi.annotation.versioning.ConsumerType;

import com.day.commons.datasource.poolservice.DataSourceNotFoundException;

@ConsumerType
public interface SQLService {
	
	/**
     * Get the Madison URL from SQL DB based on the AnchorID and InformID
     *
     * @param anchorId {@link String} AnchorId is fetched from the Sling Filter
     * 
     * @param informId {@link String} InformId is fetched from Sling Filter
     * 
     */
	public String getMadisonUrl(String anchorId, String informId) throws SQLException, DataSourceNotFoundException;
	
}
