package com.aem.demo.services;

import com.aem.demo.objects.Request;

public interface HttpUtilService {

	public String httpGet(String url);

	public <T extends Request> String httpPost(T postData, String url);

}
