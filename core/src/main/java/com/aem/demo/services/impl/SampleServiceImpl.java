package com.aem.demo.services.impl;

import java.io.IOException;

import org.apache.commons.lang.StringUtils;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.demo.config.SampleServiceConfig;
import com.aem.demo.services.SampleService;

@Component(
		immediate = true,
		service = { SampleService.class },
		enabled = true,
		property = {
				Constants.SERVICE_DESCRIPTION + "= Sample Service"
		})
@Designate(ocd=SampleServiceConfig.class)
public class SampleServiceImpl implements SampleService {
	
	private static Logger log = LoggerFactory.getLogger(SampleServiceImpl.class);
	private String name;
		
	@Activate
	protected void activate(SampleServiceConfig config) throws IOException {
		log.info("Sample Service Implemented!");
		name = config.first_name();		
	}
	
	@Override
	public String greet() {
		return name;
	}
	
	/**
	 * This method is used to read the configurations from any configuration using pid and 
	 * property name. 
	 * USE IT AS A UTIL..
	 */
	/*private String getProperty(final String pid, final String property) {
		String propertyValue = StringUtils.EMPTY;
		try {
			Bundle bundle = FrameworkUtil.getBundle(this.getClass());
		    BundleContext context = bundle.getBundleContext();
		    ServiceReference<ConfigurationAdmin> reference = context.getServiceReference(ConfigurationAdmin.class);
		    ConfigurationAdmin configAdmin = context.getService(reference);
		    Configuration conf = configAdmin.getConfiguration(pid);
		    propertyValue = (String) conf.getProperties().get(property);	// Type Cast into required Type, otherwise it will only be able to fetch String type of Value. 
		    context.ungetService(reference);
			log.info("Property: {}", property);
		} catch(Exception e) {
			log.error("Error in getProperty() :: {}", e);
		}
		return propertyValue;
	}*/
	
}
