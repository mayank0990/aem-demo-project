package com.aem.demo.services;

import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.WorkflowProcess;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.day.cq.mailer.MessageGateway;
import com.day.cq.mailer.MessageGatewayService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Reference;

/**
 * Custom Workflow Process
 */
@Slf4j
@Component(service = WorkflowProcess.class, immediate = true,
        configurationPolicy = ConfigurationPolicy.OPTIONAL,
        property = {
                Constants.SERVICE_DESCRIPTION + "=" + "Test E-mail Workflow Process",
                Constants.SERVICE_VENDOR + "=" + "Demo",
                "process.label" + "=" + "Test E-Mail Workflow Process"
        })
public class CustomWorkflowProcess implements WorkflowProcess {

    @Reference
    private MessageGatewayService messageGatewayService;

    @Override
    public void execute(WorkItem workItem, WorkflowSession workflowSession,
                        MetaDataMap metaDataMap) throws WorkflowException {
        log.info("Custom Workflow Process Executing..");
        log.info("Content Path : {}", workItem.getContentPath());

        try {
            MessageGateway<Email> messageGateway;
            Email email = new SimpleEmail();
            String emailToRecipients = "madhav0990@gmail.com";

            email.addTo(emailToRecipients);
            email.setSubject("AEM Custom Step");
            email.setFrom("mayankyadav0990@gmail.com");
            email.setMsg("This message is to inform you that the CQ content has been deleted");

            messageGateway = messageGatewayService.getGateway(Email.class);
            messageGateway.send((Email) email);
            
        } catch(EmailException e) {
            log.error("CustomWorkflowProcess : execute() : {}", e.getMessage());
        }
    }
}
