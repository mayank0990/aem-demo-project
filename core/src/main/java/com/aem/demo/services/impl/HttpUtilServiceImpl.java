package com.aem.demo.services.impl;

import java.io.IOException;
import java.util.Objects;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;

import com.aem.demo.objects.Request;
import com.aem.demo.services.HttpUtilService;
import com.google.gson.Gson;

import lombok.extern.slf4j.Slf4j;

/**
 * HTTP Service
 * 
 * @author Mayank Yadav
 *
 */
@Slf4j
@Component(immediate = true, service = HttpUtilService.class, property = {
		Constants.SERVICE_DESCRIPTION + "= Filter incoming requests and redirect" })
public class HttpUtilServiceImpl implements HttpUtilService {

	private static final String CONTENT_TYPE = "Content-Type";
	private static final String APPLICATION_JSON = "application/json";

	@Activate
	public void activate() {
		log.info("HttpUtilServiceImpl Activated!");
	}

	/**
	 * Method httpGet(...) to make a HTTP GET Call
	 * 
	 * @param url
	 * 
	 * @return
	 * 		{@link String}
	 */
	@Override
	public String httpGet(String url) {
		String response = StringUtils.EMPTY;
		try(CloseableHttpClient httpClient = HttpClients.createDefault()) {
			HttpGet httpGet = new HttpGet(url);
			HttpResponse httpResponse = httpClient.execute(httpGet);
			response = getResponse(httpResponse);			
		}
		catch (final Exception e) {
			log.error("HttpUtilService : httpGet() : Exception : {} ", e);
		}
		return response;
	}

	/**
	 * Method httpPost(...) to make HTTP Post Call
	 * 
	 * @param <T>
	 * 
	 * @param postData
	 * 
	 * @param url
	 * 
	 * @return
	 * 		{@link String}
	 */
	@Override
	public <T extends Request> String httpPost(T postData, String url) {
		try(CloseableHttpClient httpClient = HttpClients.createDefault()) {
			HttpPost httpPost = new HttpPost(url);
			httpPost.setHeader(CONTENT_TYPE, APPLICATION_JSON);
			String postDataString = StringUtils.EMPTY;
			if(postData != null) {
				postDataString = new Gson().toJson(postData);
			} 
			else if (StringUtils.isBlank(postDataString)) {
				postDataString = "{}";
			}
			StringEntity entity = new StringEntity(postDataString);
			httpPost.setEntity(entity);
			HttpResponse httpResponse = httpClient.execute(httpPost);
			return getResponse(httpResponse);
		} catch(Exception e) {
			log.error("HttpUtilService : httpPost() : Exception : {} ", e);
		}
		return null;
	}

	/**
	 * Method getResponse(...) method to Handle the Http Response.
	 * 
	 * @param response
	 * 
	 * @return {@link String}
	 * 
	 * @throws IOException
	 */
	private static String getResponse(final HttpResponse response) throws IOException {
		int status = response.getStatusLine().getStatusCode();
		if (status >= 200 && status < 300) {
			HttpEntity entity = response.getEntity();
			if (Objects.isNull(entity)) {
				return null;
			} else {
				return EntityUtils.toString(entity);
			}
		} else {
			return null;
		}
	}
}
