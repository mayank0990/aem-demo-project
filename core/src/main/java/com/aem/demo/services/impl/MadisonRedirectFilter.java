package com.aem.demo.services.impl;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.engine.EngineConstants;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.demo.services.SQLService;
import com.day.commons.datasource.poolservice.DataSourceNotFoundException;

/**
 * Madison URL Redirect Filter
 * 
 * @author Mayank Yadav
 *
 */
@Component(
		immediate = true,
		service = Filter.class,
		property = {
				Constants.SERVICE_DESCRIPTION + "= Filter incoming requests and redirect",
				EngineConstants.SLING_FILTER_SCOPE + "=" + EngineConstants.FILTER_SCOPE_REQUEST,
				"sling.filter.pattern" + "=" +  "^(.(?!.*\\.[a-zA-Z]))*$"
		})
public class MadisonRedirectFilter implements Filter {
	
	private static Logger log = LoggerFactory.getLogger(MadisonRedirectFilter.class);
	
	private String url = "";
	private String informId = "";
	private String anchorId = "";
	private String madisonUrl = "";
	
	@Reference
	SQLService sqlService;
	
	@Activate
	public void activate() {
		log.info("MadisonRedirectFilter Filter Service Activated!");
	}
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		
	}

	@Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
 
        if (!(request instanceof SlingHttpServletRequest) ||
                !(response instanceof SlingHttpServletResponse)) {
            // Not a SlingHttpServletRequest/Response, so ignore.
            chain.doFilter(request, response); // This line would let you proceed to the rest of the filters. 
            return;
        }
 
        final SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) request;
        final SlingHttpServletResponse slingResponse = (SlingHttpServletResponse) response;
        
        this.url = slingRequest.getPathInfo();
        this.anchorId = slingRequest.getParameter("anchorId");
        this.informId = slingRequest.getParameter("id");
        
        try {
        	if(informId != null && anchorId != null) {
        		log.info("FIRST CASE");
        		madisonUrl = sqlService.getMadisonUrl(anchorId, informId);
        		log.info("Madison URL (First Case) :: " + madisonUrl);
        		if(madisonUrl != "") {
        			slingResponse.setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY);
        			slingResponse.setHeader("Location", madisonUrl);
        			slingResponse.flushBuffer();
        		} else {
        			slingResponse.setStatus(HttpServletResponse.SC_NOT_FOUND);
        			slingResponse.flushBuffer();
        		}
        	} else if(anchorId != null && informId == null) {
        		log.info("SECOND CASE");
        		String[] urlParts = url.split("/");
				informId = urlParts[urlParts.length-1];
				if(informId != null && anchorId != null) {
					madisonUrl = sqlService.getMadisonUrl(anchorId, informId);
					log.info("Madison URL (Second Case) :: " + madisonUrl);
	        		if(madisonUrl != "") {
	        			slingResponse.setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY);
	        			slingResponse.setHeader("Location", madisonUrl);
	        			slingResponse.flushBuffer();
	        		} else {
	        			slingResponse.setStatus(HttpServletResponse.SC_NOT_FOUND);
	        			slingResponse.flushBuffer();
	        		}
				}
        	} else {
        		log.info("No Cases Found!!");
        	}
		} catch (SQLException | DataSourceNotFoundException e) {
			log.info("SQLException {}", e);
		}
        
        // To Proceed with the rest of the Filter chain
        chain.doFilter(request, response);
    }
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub	
	}	
}
