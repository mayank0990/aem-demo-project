package com.aem.demo.services;

import org.osgi.annotation.versioning.ConsumerType;

@ConsumerType
public interface SampleService {
	
	public String greet();

}
