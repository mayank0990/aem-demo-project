package com.aem.demo.services;

import org.apache.sling.api.SlingHttpServletRequest;

public interface RequestParserService {
	
	/**
	 * Returns a mapped object for any POST Data Object
	 * 
	 * @param <T>
	 * @param request
	 * @param element
	 * @return
	 */
	public <T> Object getMappedObject(final SlingHttpServletRequest request, final Class<T> element);

}
