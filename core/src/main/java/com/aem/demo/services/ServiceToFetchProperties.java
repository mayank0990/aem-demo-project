package com.aem.demo.services;

import org.osgi.annotation.versioning.ConsumerType;

@ConsumerType
public interface ServiceToFetchProperties {
	
	/**
	 * This method is used to read the configurations from any configuration using pid and 
	 * property name. <br>
	 * <b>USE IT AS A UTIL..</b>
	 * 
	 * @param pid
	 * 
	 * @param prop
	 * 
	 * @return {@link String} Value of the property
	 */
	public String fetchConfigProperty(String pid, String prop);

}
