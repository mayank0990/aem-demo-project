package com.aem.demo.services.impl;

import org.apache.commons.lang.StringUtils;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.demo.services.ServiceToFetchProperties;

/**
 * This service is used to fetch the property from any service using the PID
 * 
 * @author Mayank
 *
 */
@Component(service = {ServiceToFetchProperties.class}, immediate = true)
public class ServiceToFetchPropertiesImpl implements ServiceToFetchProperties {

	/** Default Logger */
	private static final Logger LOGGER = LoggerFactory.getLogger(ServiceToFetchPropertiesImpl.class);
		
	@Reference
	private ConfigurationAdmin configAdmin;
	
	@Activate
	protected void activate() {
		LOGGER.info("ServiceToFetchPropertiesImpl Activated!");
	}
	
	@Override
	public String fetchConfigProperty(String pid, String prop) {
		String propValue = StringUtils.EMPTY;
		try {
			Configuration conf = configAdmin.getConfiguration(pid);
			propValue = (String) conf.getProperties().get(prop);
		} catch(Exception e) {
			LOGGER.error("Error: in displayData() :: {}", e);
		}
		return propValue;
	}
}
