package com.aem.demo.services.impl;

import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;

import com.aem.demo.services.RequestParserService;
import com.google.gson.Gson;

import lombok.extern.slf4j.Slf4j;

/**
 * Request Parser Service Implementation
 * 
 * @author Mayank Yadav
 *
 */
@Slf4j
@Component(immediate = true, service = RequestParserService.class, property = {
		Constants.SERVICE_DESCRIPTION + "= Request Parser Service Implementation" })
public class RequestParserServiceImpl implements RequestParserService {
	
	@Activate
	protected void activate() {
		log.info("RequestParserService Activated!");
	}
	
	/**
	 * {@inheritDoc}
	 */
	public <T> Object getMappedObject(final SlingHttpServletRequest request, final Class<T> type) {
		Object obj = null;
		try {
			String data = IOUtils.toString(request.getReader());
			if(StringUtils.isNotBlank(data)) {
				obj = new Gson().fromJson(data, type);
			}
		} catch (IOException ioException) {
			log.error(
					"RequestParserService : getMappedObject() : IO Exception occured while getting Object from request {}",
					ioException);
		}		
		return obj;
	}

}