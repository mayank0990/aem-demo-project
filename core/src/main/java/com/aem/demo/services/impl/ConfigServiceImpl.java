package com.aem.demo.services.impl;

import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(service = ConfigServiceImpl.class, immediate = true, 
property = {
		Constants.SERVICE_DESCRIPTION + "= Here you can configure the collection components properties"
})
@Designate(ocd = ConfigServiceImpl.Config.class )
public class ConfigServiceImpl {
	
	protected final Logger LOGGER = LoggerFactory.getLogger(ConfigServiceImpl.class);

    public static final int COLLECTIONS_DEFAULT_LIMIT = 100;
	public static final String COLLECTIONS_STRING_DEFAULT_LIMIT = "100";
    public static final Boolean COLLECTION_LOGGER_DEFAULT = false;
    
	private static int collectionLimit;
	private static int contactCollectionLimit;
	private static int eventCollectionLimit;
    private static boolean isCollectionLoggerEnabled;
    
    @ObjectClassDefinition(name = "Config Service Impl ", 
    		description = "Here you can configure the collection components properties")
    @interface Config {
    	@AttributeDefinition(name = "Collection logger", 
				description = "Collection logger",
				type = AttributeType.BOOLEAN)
    	public boolean collectionLogger();
    	
    	@AttributeDefinition(name = "Collection limit", 
				description = "Collection limit for retrieving Pages or PDFs",
				type = AttributeType.INTEGER)
    	public int collectionLimit() default COLLECTIONS_DEFAULT_LIMIT;
    	
    	@AttributeDefinition(name = "Contact Collection limit", 
				description = "Contact Collection limit",
				type = AttributeType.INTEGER)
    	public int contactCollectionLimit() default COLLECTIONS_DEFAULT_LIMIT;
    	
    	@AttributeDefinition(name = "Event Collection limit", 
				description = "Event Collection limit",
				type = AttributeType.INTEGER)
    	public int eventCollectionLimit() default COLLECTIONS_DEFAULT_LIMIT;    	
    }
    
    @Activate
    @Modified
    protected void activate(ConfigServiceImpl.Config config) {
    	collectionLimit = config.collectionLimit();
		contactCollectionLimit = config.contactCollectionLimit();
		eventCollectionLimit = config.eventCollectionLimit();
        isCollectionLoggerEnabled = config.collectionLogger();
        
        LOGGER.info("Limit: " + collectionLimit);
        LOGGER.info("Contact Collection Limit: " + contactCollectionLimit);
        LOGGER.info("Event Collection Limit: " + eventCollectionLimit);
        LOGGER.info("Enable Logger: " + isCollectionLoggerEnabled);
    }
}
