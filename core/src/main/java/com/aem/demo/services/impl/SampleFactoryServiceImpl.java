package com.aem.demo.services.impl;

import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

import com.aem.demo.services.SampleFactoryService;

/**
 * Sample Factory Service using OSGi Annotations
 */
@Component(service = SampleFactoryService.class, immediate = true, property = {
		Constants.SERVICE_DESCRIPTION + "= Sample Factory Service Implementation"
})
@Designate(ocd = SampleFactoryServiceImpl.Config.class, factory = true)
public class SampleFactoryServiceImpl implements SampleFactoryService {
	
	private String name;
	
	@ObjectClassDefinition(name = "Simple Factory Service ", description = "")
    @interface Config {
    	@AttributeDefinition(name = "Name", 
    						description = "Enter your Name",
    						type = AttributeType.STRING)
    	public String getName();
    }
	
	@Activate
	@Modified
	public void activate(final Config context) {
		name = context.getName();
	}
	
	@Override
	public String getName() {
		return name;
	}
}
