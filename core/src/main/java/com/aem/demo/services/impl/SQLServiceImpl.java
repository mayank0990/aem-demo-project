package com.aem.demo.services.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.demo.services.SQLService;
import com.day.commons.datasource.poolservice.DataSourceNotFoundException;
import com.day.commons.datasource.poolservice.DataSourcePool;

/**
 * SQL Service Implementation
 * 
 * @author Mayank
 *
 */
@Component(
		immediate = true,
		service = { SQLService.class },
		enabled = true,
		property = {
				Constants.SERVICE_DESCRIPTION + "= SQL Service Implementation"
		})
public class SQLServiceImpl implements SQLService {

	/*Static Logger*/
	private static Logger logger = LoggerFactory.getLogger(SQLServiceImpl.class);

	@Reference
	DataSourcePool dataSourcePool;

	PreparedStatement preparedStatement = null;

	@Activate
	protected void actiavte() {
		logger.info("SQLServiceImpl Activated!");
	}

	private Connection getConnection() throws SQLException, DataSourceNotFoundException {
		final DataSource dataSource = (DataSource) dataSourcePool.getDataSource("sqldatasource");
		final Connection connection = dataSource.getConnection();
		return connection;
	}

	@Override
	public String getMadisonUrl(String anchorId, String informId) throws SQLException, DataSourceNotFoundException {
		
		final Connection connection = getConnection();
		String madisonUrl = "";
		
		if(anchorId != StringUtils.EMPTY && informId != StringUtils.EMPTY) {
			String query = 
					"SELECT madisonUrl FROM UrlManipulation WHERE anchorId='" 
					+ anchorId + "' AND informId='" + informId + "'";			
			try {
				if(connection != null) {
					preparedStatement = connection.prepareStatement(query);
					final ResultSet rs = preparedStatement.executeQuery();

					while(rs.next()) {
						madisonUrl = rs.getString("madisonUrl");
					}
					return madisonUrl;
				}

			} catch(Exception e) {
				logger.info("Exception in getMadisonUrl {} ", e);
				
			} finally {
				if(connection != null) {
					connection.close();
				}
				if(preparedStatement != null) {
					preparedStatement.close();
				}
			}
		} else {
			logger.info("Missing Parameters!!");
		}
		return madisonUrl;
	}
}
