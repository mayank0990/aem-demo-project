package com.aem.demo.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.commons.ReferenceSearch;

public class DITAUtils {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DITAUtils.class);
	
	/**
     * Method to read any full path of the DITA and get the related cq:Page.
     *
     * @param ditaPath
     * @param resolver
     * @return
     */
    public static String getPageFromXrefDita(final String ditaPath, final ResourceResolver resolver) {

        if(null == resolver || StringUtils.isBlank(ditaPath)) {
            return StringUtils.EMPTY;
        }
        ReferenceSearch referenceSearch = new ReferenceSearch();
        referenceSearch.setExact(true);
        referenceSearch.setHollow(false);
        Collection<ReferenceSearch.Info> resultSet = referenceSearch.search(resolver, ditaPath).values();
        if(resultSet.isEmpty()) {
        	LOGGER.info("Result Set EMPTY");
            return ditaPath;
        }
        List<Page> refPages = new ArrayList<>(resultSet.size());
        for (ReferenceSearch.Info info : resultSet) {
            Page topicPage = info.getPage();
            refPages.add(topicPage);
        }
        refPages.sort(new PageSortComparator());
        return refPages.get(0).getPath();
    }
    
    private static class PageSortComparator implements Comparator<Page> {

        @Override
        public int compare(Page e1, Page e2) {
            return e2.getLastModified().compareTo(e1.getLastModified());
        }
    }

}
