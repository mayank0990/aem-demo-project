package com.aem.demo.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.jcr.RepositoryException;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.dam.api.AssetManager;
import com.day.crx.JcrConstants;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.MalformedJsonException;

public final class DemoUtil {

	// Private Constructor to avoid unnecessary instantiation of the class
	private DemoUtil() {}

	private static Logger LOGGER = LoggerFactory.getLogger(DemoUtil.class);
	private static final String UTF_8_ENCODING = "UTF-8";

	/**
	 * Returns {@link ResourceResolver} of the given sub-service. It returns null in case given
	 * {@link ResourceResolverFactory} and subService are null.
	 * 
	 * @param resourceResolverFactory
	 *            {@link ResourceResolverFactory}
	 * @param subService
	 *            {@link String} sub-service defined in Sling Apache User Mapping configuration
	 * @return {@link ResourceResolver}
	 * 
	 * */
	public static ResourceResolver getResourceResolver(final ResourceResolverFactory resourceResolverFactory,
			final String subService) {
		ResourceResolver resourceResolver = null;
		if(null != resourceResolverFactory && null != subService) {
			try {
				final Map<String,Object> authInfo = new HashMap<>();
				authInfo.put(ResourceResolverFactory.SUBSERVICE, subService);
				resourceResolver = resourceResolverFactory.getServiceResourceResolver(authInfo);				
			} catch(final LoginException loginException) {
				LOGGER.error(
						"DemoUtil : getResourceResolver() : Exception while getting resourceResolver for subService {} : {} ",
						subService, loginException); 
			}
		}
		return resourceResolver;
	}

	/**
	 * Method is used to fetch the List<T> from the JSON file stored in Dam. 
	 * 
	 * @param <T>
	 * 
	 * @param resourceResolver
	 * 
	 * @param filePath
	 * 
	 * @param dtoType
	 * 
	 * @return
	 * 		{@link List<T>}
	 */
	public static <T> List<T> getJSONDataFromCRX(final ResourceResolver resourceResolver, final String filePath, 
			final Class<T> dtoType) {
		List<T> excelData = new ArrayList<>();
		if(StringUtils.isNotBlank(filePath)) {
			LOGGER.debug("File Path :: {}", filePath);
			Resource jsonRes = resourceResolver.getResource(filePath);
			if(Objects.nonNull(jsonRes)) {
				ValueMap resProps = jsonRes.getValueMap();
				InputStream jsonStream = resProps.get(JcrConstants.JCR_DATA, InputStream.class);
				JsonElement element = new JsonParser().parse(new InputStreamReader(jsonStream));
				String jsonStr = element.toString();
				Gson gson = new Gson();
				excelData = gson.fromJson(jsonStr, getType(List.class, dtoType));
			}
		}
		return excelData;
	}

	/**
	 * Method to get the Type on Runtime
	 * 
	 * @param rawClass
	 * 
	 * @param parameterClass
	 * 
	 * @return
	 * 		{@link Type}
	 */
	private static Type getType(final Class<?> rawClass, final Class<?> parameterClass) {
		return new ParameterizedType() {

			@Override
			public Type[] getActualTypeArguments() {
				return new Type[]{parameterClass};
			}

			@Override
			public Type getRawType() {
				return rawClass;
			}

			@Override
			public Type getOwnerType() {
				return null;
			}
		};
	}

	/**
	 * Method is used upload a JSON file to DAM.
	 * 
	 * @param {@link InputStream}
	 * 
	 * @param fileName
	 * 
	 * @return
	 * 		{@link String}
	 * 
	 * @throws RepositoryException 
	 */
	public static String writeOrUpdateJSONinDam(final ResourceResolver resourceResolver, 
			final InputStream is, final String filePath) throws PersistenceException {
		AssetManager assetMgr = resourceResolver.adaptTo(AssetManager.class);
		if(Objects.nonNull(assetMgr) && StringUtils.isNotBlank(filePath)) {
			Resource resource = resourceResolver.getResource(filePath);
			if(Objects.nonNull(resource)) {
				resourceResolver.delete(resource);
				resourceResolver.commit();
				assetMgr.createAsset(filePath, is,"application/json", true);
			} else {
				assetMgr.createAsset(filePath, is,"application/json", true);
			}
		} else {
			LOGGER.info("Asset Manager might be null or empty!!");
		}
		return filePath;
	}

	public static JSONObject makeResponseObjectsForServlet(int status, String success, String message, String data) {
		JSONObject responseForServlet = new JSONObject();
		try {
			responseForServlet.put("status", status);
			responseForServlet.put("success",success);
			responseForServlet.put("message", message);
			if(data != null && !data.isEmpty()) {
				responseForServlet.put("data", data);
			} else {
				responseForServlet.put("data", data);
			}
			return responseForServlet;
		}catch(Exception err) {
			LOGGER.error("Error DemoUtil :: makeResponseObjectsForServlet() {}",  err);
		}
		return responseForServlet;
	}

	/**
	 * Method to return the mapped object from request.
	 * 
	 * @param <T>
	 * 
	 * @param slingHttpServletRequest
	 * 
	 * @param type
	 * 
	 * @return
	 * 
	 * @throws IOException
	 */
	public static <T> Object getObjectFromRequest(final SlingHttpServletRequest slingHttpServletRequest,
			final Class<T> type) throws IOException {
		boolean isMultipart = ServletFileUpload.isMultipartContent(slingHttpServletRequest);
		Object object = null;
		if (slingHttpServletRequest != null && !isMultipart) {
			try (final BufferedReader bufferedReader = new BufferedReader(
					new InputStreamReader(slingHttpServletRequest.getInputStream(), UTF_8_ENCODING))) {
				String jsonString = StringUtils.EMPTY;
				jsonString = bufferedReader.readLine();
				object = new Gson().fromJson(jsonString, type);
			} catch (IllegalStateException | JsonSyntaxException | MalformedJsonException exception) {
				LOGGER.error(
						"DemoUtil : getObjectFromRequest() : JsonSyntaxException Exception occured while getting Object from request {}",
						exception);

			} catch (final IOException ioException) {
				LOGGER.error(
						"DemoUtil : getObjectFromRequest() : IO Exception occured while getting Object from request {}",
						ioException);
			}
		} else if (isMultipart) {
			JSONObject jo = new JSONObject();
			String value = StringUtils.EMPTY;
			if(slingHttpServletRequest != null) {
				final Map<String, RequestParameter[]> params = slingHttpServletRequest.getRequestParameterMap();
				try {
					for (final Map.Entry<String, RequestParameter[]> pairs : params.entrySet()) {
						final String k = pairs.getKey();
						final RequestParameter[] pArr = pairs.getValue();
						final RequestParameter param = pArr[0];
						final InputStream stream = param.getInputStream();
						if (param.isFormField()) {
							value = Streams.asString(stream);
							jo.put(k, value);
							LOGGER.debug("Form field {} with key value {} detected.", k, value);
						} else {
							LOGGER.debug("File field {} with file name {} detected.", k, param.getFileName());
						}
					}
					object = new Gson().fromJson(jo.toString(), type);
				} catch (Exception e) {
					LOGGER.error("Error in servlet {}", e.getMessage());
				}
			}
		}
		return object;
	}
}
