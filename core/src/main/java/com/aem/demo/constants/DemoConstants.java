package com.aem.demo.constants;

public class DemoConstants {

	public static final String SAMPLE_CONSTANT = "sampleConstant";
	public static final String DEMO_READ_SUBSERVICE = "read";
	public static final String DEMO_WRITE_SUBSERVICE = "write";

}
