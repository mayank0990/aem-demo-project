package com.aem.demo.userreg.servlets;

import java.io.IOException;
import java.util.Objects;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.demo.userreg.models.UserInfo;
import com.aem.demo.userreg.services.UserRegistrationService;
import com.google.gson.Gson;

/**
 * User Registration Servlet
 *  
 * @author Mayank
 *
 */
@Component(service = Servlet.class, name = "User Registration Servlet", property = {
		Constants.SERVICE_DESCRIPTION + "= User Registration Servlet", 
		"sling.servlet.methods=" + HttpConstants.METHOD_POST,
		"sling.servlet.paths=" + "/bin/user/reg" })
public class UserRegServlet extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 7270861655022997150L;

	protected static final String REGISTER_PATH = "";

	private static final Logger LOGGER = LoggerFactory.getLogger(UserRegServlet.class);

	@Reference
	protected UserRegistrationService userRegService;

	@Override
	protected void doPost(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
			throws ServletException, IOException {
		try {
			boolean status = false;
			Gson gson = new Gson();
			UserInfo user = null;
			String data = IOUtils.toString(request.getReader());
			if(StringUtils.isNotBlank(data)) {
				user = gson.fromJson(data, UserInfo.class);	
			}
			if(!Objects.isNull(user)) {
				status = userRegService.createUser(user);
			}
			LOGGER.info("Status: " + status);
			if(status) {
				response.setStatus(SlingHttpServletResponse.SC_OK);
				response.flushBuffer();
			} else {
				response.setStatus(SlingHttpServletResponse.SC_NOT_FOUND);
				response.flushBuffer();
			}
		} catch(Exception e) {
			LOGGER.error("Error in doPost() :: UserRegServlet {}", e);
		}

	}
}