package com.aem.demo.userreg.services;

import com.aem.demo.userreg.models.UserInfo;

public interface UserRegistrationService {
	
	/**
	 * Create User Method
	 * 
	 * @param username
	 * 
	 * @param password
	 * 
	 * @param email
	 * 
	 * @return {@link boolean}
	 */
	public boolean createUser(UserInfo userInfo);

}
