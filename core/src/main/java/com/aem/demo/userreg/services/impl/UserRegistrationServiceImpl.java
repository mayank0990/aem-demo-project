package com.aem.demo.userreg.services.impl;

import java.security.Principal;

import javax.jcr.PropertyType;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.jcr.ValueFactory;

import org.apache.jackrabbit.api.JackrabbitSession;
import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.Group;
import org.apache.jackrabbit.api.security.user.User;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.demo.constants.DemoConstants;
import com.aem.demo.userreg.models.UserInfo;
import com.aem.demo.userreg.services.UserRegistrationService;
import com.aem.demo.utils.DemoUtil;

/**
 * User Registration Service Implementation
 * 
 * @author Mayank
 *
 */
@Component(immediate = true, service = UserRegistrationService.class, property = {
		Constants.SERVICE_DESCRIPTION + "= User Registration Service Impl" })
public class UserRegistrationServiceImpl implements UserRegistrationService {
	
	/*Static Logger*/
	private static Logger LOGGER = LoggerFactory.getLogger(UserRegistrationServiceImpl.class);
	
	private static final String INTERMEDIATE_PATH = "demo-users";
	private static final String PROFILE_PATH = "./profile/";
	private static final String GROUP_NAME = "Demo";
	
	@Reference
	private ResourceResolverFactory resolverFactory;
	
	private ResourceResolver resolver;
	private Session session;
	
	@Activate
	protected void activate() {
		LOGGER.info("UserRegistrationServiceImpl Activated!");	
	}
	
	@Override
	public boolean createUser(UserInfo userInfo) {
		boolean status = false;
		try {
			resolver = DemoUtil.getResourceResolver(resolverFactory, DemoConstants.DEMO_READ_SUBSERVICE);
			session = resolver.adaptTo(Session.class);
			UserManager userManager = resolver.adaptTo(UserManager.class);
			
			if (session != null && session instanceof JackrabbitSession) { 
				ValueFactory valueFactory = session.getValueFactory();
				Value email = valueFactory.createValue(userInfo.getEmail(), PropertyType.STRING);
				Value firstName = valueFactory.createValue(userInfo.getFirstName(), PropertyType.STRING);
				Value lastName = valueFactory.createValue(userInfo.getLastName(), PropertyType.STRING);
				
				/**
				 * Checking Existing Group,
				 * If it's there, Initialize it with the previous one
				 * otherwise Create a New One. 
				 */
				Group group = null;
				if(userManager.getAuthorizable(GROUP_NAME) == null) {
					group = userManager.createGroup(GROUP_NAME);				
				} else {					
					group = (Group)(userManager.getAuthorizable(GROUP_NAME));
				}
				
				/**
				 * Creating New User 
				 */
				User user = null;
				Authorizable authUser = null;
				if(userManager.getAuthorizable(userInfo.getEmail()) == null) {
					user = userManager.createUser(userInfo.getEmail(), userInfo.getPassword(), 
							SimplePrincipal(userInfo.getEmail()), INTERMEDIATE_PATH);	
					authUser = (Authorizable) user;
					user.setProperty(PROFILE_PATH + "email", email);
					user.setProperty(PROFILE_PATH + "firstName", firstName);
					user.setProperty(PROFILE_PATH + "lastName", lastName);
					group.addMember(authUser);				
				}
				
				if(session.hasPendingChanges()) {
					session.save();
					status = true;
				}
			}	
		} catch(Exception e) {
			LOGGER.error("Error in createUser() :: UserRegistrationServiceImpl {}  ", e);
		} finally {
			if(session != null && session.isLive()) {
				session.logout();
			}
		}
		return status;
	}
	
	/**
	 * Method to Create Principal Object
	 * @param arg
	 * @return
	 */
	private static Principal SimplePrincipal(String arg) {
		return new Principal() {
			public String getName() {
				return arg;
			}
		};
	}

}
