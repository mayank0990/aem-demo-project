package com.aem.demo.userreg.services;

import java.io.IOException;
import java.io.PrintWriter;

import javax.jcr.RepositoryException;
import javax.jcr.SimpleCredentials;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.jackrabbit.api.security.user.User;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.auth.core.spi.AuthenticationFeedbackHandler;
import org.apache.sling.auth.core.spi.AuthenticationHandler;
import org.apache.sling.auth.core.spi.AuthenticationInfo;
import org.apache.sling.jcr.api.SlingRepository;
import org.apache.sling.jcr.resource.api.JcrResourceConstants;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.crx.security.token.TokenUtil;

@Component(immediate = true, enabled = true,
		property = { Constants.SERVICE_DESCRIPTION + "=" + "Custom Authentication Handler",
					 AuthenticationHandler.PATH_PROPERTY + "=" + "/customAuthHandler",
					 AuthenticationHandler.TYPE_PROPERTY + "=" + HttpServletRequest.FORM_AUTH })
public class CustomAuthenticationHandler implements AuthenticationHandler, AuthenticationFeedbackHandler {

	/**Static Logger*/
	private final Logger LOGGER = LoggerFactory.getLogger(CustomAuthenticationHandler.class);
	
	@Reference
	private SlingRepository slingRepository;
	
	@Reference
	private ResourceResolverFactory resourceResolverFactory;

	@Activate
	public void activate() {
		LOGGER.info("CustomAuthenticationHandler activated!");
	}

	@Override
	public void authenticationFailed(HttpServletRequest req, HttpServletResponse res, AuthenticationInfo arg2) {
		try {			
			res.setStatus(HttpServletResponse.SC_NOT_FOUND);
			res.flushBuffer();
		} catch (IOException e) {
			LOGGER.error("Exception in Auth Failed: ",e);
		}

	}

	@Override
	public boolean authenticationSucceeded(HttpServletRequest req, HttpServletResponse res, AuthenticationInfo arg2) {
		try {
			PrintWriter out = res.getWriter();
			String username = req.getParameter("username");
			TokenUtil.createCredentials(req, res, slingRepository,
					username, true); 	// It will throw Repository Exception if we'll not Whitelist the bundle
										// TokenUtil.createCredentials() method will create a token node under the user
										// after successful authentication & if we don't whitelist the bundle, then it throws Exception. 
			res.setContentType("text/html");
			res.setStatus(HttpServletResponse.SC_OK);
			out.println("Success");
			res.flushBuffer();
		} catch (IOException | RepositoryException e) {
			LOGGER.error("Exception in Auth Succeeded: ",e);
		}

		return true;
	}

	@Override
	public void dropCredentials(HttpServletRequest arg0, HttpServletResponse arg1) throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public AuthenticationInfo extractCredentials(HttpServletRequest request, HttpServletResponse response) {
		String user = request.getParameter("username");
		String pwd = request.getParameter("pass");
		AuthenticationInfo authInfo = null;
		try {
			SimpleCredentials creds = new SimpleCredentials(user, pwd.toCharArray());
			authInfo = new AuthenticationInfo(HttpServletRequest.FORM_AUTH, creds.getUserID());
			authInfo.put(JcrResourceConstants.AUTHENTICATION_INFO_CREDENTIALS, creds);
			
			// Also, we can extract User Node in CRX
			ResourceResolver resolver = resourceResolverFactory.getResourceResolver(authInfo);
			User jcrUser = resolver.adaptTo(User.class);
			LOGGER.info("User Path: " + jcrUser.getPath());
			
		} catch(Exception e) {
			LOGGER.error("Error in extractCredentials() method :: {} ", e);
		}
		return authInfo;
	}

	@Override
	public boolean requestCredentials(HttpServletRequest arg0, HttpServletResponse arg1) throws IOException {
		// TODO Auto-generated method stub
		return false;
	}

}
