package com.aem.demo.config;

import org.apache.commons.lang.StringUtils;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name="Simple Scheduler Task Config", description="Simple demo for cron-job like task with properties")
public @interface SimpleSchedulerConfig {

	@AttributeDefinition(name = "Cron-job expression")
	public String scheduler_expression() default "0/30 * * * * ?";

	@AttributeDefinition(name = "Concurrent task",
			description = "Whether or not to schedule this task concurrently")
	public boolean scheduler_concurrent() default false;
	
	@AttributeDefinition(name = "A parameter",
			description="can be configured in /system/console/configMgr")
	public String myParameter() default StringUtils.EMPTY;
	
}
