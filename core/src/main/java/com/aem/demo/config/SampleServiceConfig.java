package com.aem.demo.config;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name="Sample Service to extract properties from other Bundle", description="Configure Sample Service")
public @interface SampleServiceConfig {
	
	@AttributeDefinition(name="Your Name ", description="")
	public String first_name();

}
