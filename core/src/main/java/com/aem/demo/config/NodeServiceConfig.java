package com.aem.demo.config;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name="Node Iterator Service", description="")
public @interface NodeServiceConfig {

	@AttributeDefinition(name="Parent Path: ", description="", type=AttributeType.STRING)
	public String parentPagePath();

	@AttributeDefinition(name="Node Type: ", description="", type=AttributeType.STRING)
	public String type();

	@AttributeDefinition(name="Node Property: ", description="", type=AttributeType.STRING)
	public String property();

}