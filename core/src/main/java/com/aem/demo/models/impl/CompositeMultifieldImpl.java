package com.aem.demo.models.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.InjectionStrategy;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.export.json.ExporterConstants;
import com.aem.demo.models.CompositeMultifield;

@Model(adaptables = { SlingHttpServletRequest.class }, adapters = {
		CompositeMultifield.class }, resourceType = CompositeMultifieldImpl.RESOURCE_TYPE)
@Exporter(name = ExporterConstants.SLING_MODEL_EXPORTER_NAME, extensions = ExporterConstants.SLING_MODEL_EXTENSION)
public class CompositeMultifieldImpl implements CompositeMultifield {
	
	/** Default Logger */
	private static final Logger LOGGER = LoggerFactory.getLogger(CompositeMultifieldImpl.class);

	protected static final String RESOURCE_TYPE = "aem-demo-project/components/content/composite-multifield";

	@ValueMapValue
	@Default(values = StringUtils.EMPTY)
	private String title;

	@ChildResource(injectionStrategy = InjectionStrategy.OPTIONAL)
	private Resource samples;

	private final List<MultifieldContent> contentList = new ArrayList<>();

	/**
	 * init() method of LinkField
	 */
	@PostConstruct
	protected void init() {

		if (Objects.nonNull(samples)) {
			for (final Resource sampleResource : samples.getChildren()) {
				final MultifieldContent content = sampleResource.adaptTo(MultifieldContent.class);
				if (Objects.nonNull(content)) {
					LOGGER.info("Content List: {}", contentList);
					contentList.add(content);
				}
			}
		}
	}

	/**
	 * @return List of {@link MultifieldContent} empty list if nothing is authored.
	 */
	@Override
	public List<MultifieldContent> getContentList() {
		return contentList;
	}

}
