package com.aem.demo.models.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.InjectionStrategy;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

@Model(adaptables = Resource.class)
public class MultifieldContent {
	
	@ValueMapValue
	@Default(values = StringUtils.EMPTY)
	private String rteText;
	
	@ValueMapValue
	@Default(values = StringUtils.EMPTY)
	private String path;

	@ChildResource(injectionStrategy = InjectionStrategy.OPTIONAL)
	private Resource links;

	@ChildResource(injectionStrategy = InjectionStrategy.OPTIONAL)
	private Resource imagesList;

	@ChildResource(injectionStrategy = InjectionStrategy.OPTIONAL)
	private Resource sampleList;

	@ChildResource(injectionStrategy = InjectionStrategy.OPTIONAL)
	private Resource demoList;

	private final List<LinkField> linkFieldList = new ArrayList<>();
	private final List<String> imagesArrayList = new ArrayList<>();
	private final List<String> sampleArrayList = new ArrayList<>();

	/**
	 * init() method of LinkField
	 */
	@PostConstruct
	protected void init() {
		if (Objects.nonNull(links)) {
			for (final Resource linksResource : links.getChildren()) {
				final LinkField linkField = linksResource.adaptTo(LinkField.class);
				if (Objects.nonNull(linkField)) {
					linkFieldList.add(linkField);
				}
			}
		}
		
		if(Objects.nonNull(imagesList)) {
			for(final Resource imageResource : imagesList.getChildren()) {
				if(Objects.nonNull(imageResource) && imageResource instanceof Resource) {
					imagesArrayList.add(imageResource.getValueMap().get("image", String.class));
				}
			}
		}
	}
		
	/**
	 * @return {@link String} RTE Text
	 */
	public String getRteText() {
		return rteText;
	}

	/**
	 * @return Path List
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @return List of {@link LinkField} representing links, empty list if nothing
	 *         is authored.
	 */
	public List<LinkField> getLinkFieldList() {
		return linkFieldList;
	}

	/**
	 * @return List of {@link String} representing images, empty list if nothing
	 * 
	 */
	public List<String> getImagesArrayList() {
		return imagesArrayList;
	}
}
