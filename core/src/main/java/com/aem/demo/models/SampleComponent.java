package com.aem.demo.models;

import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.scripting.sightly.pojo.Use;

import javax.script.Bindings;

@Getter
public class SampleComponent implements Use {

    private String calculatedValue;
    private String path;
    private String userName;

    @Override
    public void init(Bindings bindings) {
        calculatedValue = (String) bindings.get("param1");
        Resource resource = (Resource) bindings.get("resource");
        path = resource.getPath();
        userName = resource.getResourceResolver().getUserID();
    }
}
