package com.aem.demo.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;


@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public interface TestModel {
	
	@Inject
	public Image getImage();
	
	@PostConstruct
	default void init() {
		
	}
	
	@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
	class Image {
		
		@Inject
		private String name;
		
		@PostConstruct
		protected void init() {
			
		}
				
	}
}