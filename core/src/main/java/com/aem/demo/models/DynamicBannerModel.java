package com.aem.demo.models;

import org.osgi.annotation.versioning.ConsumerType;

@ConsumerType
public interface DynamicBannerModel {
	
	public String getImagePath();
	
}
