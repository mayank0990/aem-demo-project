package com.aem.demo.models.impl;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.demo.models.DynamicBannerModel;
import com.aem.demo.objects.DynamicBanner;
import com.google.gson.Gson;

/**
 * This component is used to change banner from request params. 
 * 
 * @author Mayank
 *
 */
@Model(adaptables = SlingHttpServletRequest.class, adapters = DynamicBannerModel.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class DynamicBannerModelImpl implements DynamicBannerModel {

	/** Default Logger */
	private static final Logger LOGGER = LoggerFactory.getLogger(DynamicBannerModelImpl.class);

	@ValueMapValue
	@Default(values = "")
	private String defaultImage;

	@ValueMapValue
	@Default(values = "{}")
	private String[] listofItems;

	@SlingObject
	private SlingHttpServletRequest request;
		
	private String imagePath;

	@PostConstruct
	protected void init() {
		try {
			Gson gson = new Gson();
			String paramValue = request.getParameter("param");
			if(StringUtils.isNotBlank(paramValue)) {
				for (String str : listofItems) {
					DynamicBanner obj = gson.fromJson(str, DynamicBanner.class);
					if (obj.getEntity().equalsIgnoreCase(paramValue)) {
						imagePath = obj.getImagePath();				
						break;
					} else {
						imagePath = defaultImage;
						LOGGER.info("Image Path (else block): {}", imagePath);
					}
				}
			} else {
				imagePath = defaultImage;
			}
		} catch(Exception e) {
			LOGGER.error("Error in init() method of DynamicBannerModeImpl :: {}",e);
		}
	}
	
	@Override
	public String getImagePath() {
		return imagePath;
	}
}
