package com.aem.demo.models;

import java.util.List;

import com.aem.demo.models.impl.MultifieldContent;

public interface CompositeMultifield {

	public List<MultifieldContent> getContentList();

}
