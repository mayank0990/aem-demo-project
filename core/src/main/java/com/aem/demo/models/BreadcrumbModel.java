package com.aem.demo.models;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.wcm.api.Page;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.query.SlingQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.*;

import static org.apache.sling.query.SlingQuery.$;

/**
 * Breadcrumb Model with implementation of Lombok
 *
 * @author Mayank
 */
@Slf4j
@Model(adaptables = SlingHttpServletRequest.class)
public class BreadcrumbModel {

    private static final String CQ_PAGE = "cq:Page";
    private static final String HTML_EXTN = ".html";

    @SlingObject
    private SlingHttpServletRequest request;

    @SlingObject
    private ResourceResolver resourceResolver;

    @ScriptVariable
	private Page currentPage;

    @Getter
    private Map<String, String> breadcrumbMap;

    /**
     * Init method of Model
     */
    @PostConstruct
    protected void init() {
        try {
        	log.info("Breadcrumb Init Called!");
        	ValueMap properties = currentPage.getProperties();
        	String excludeProp = properties.get("excludeFromBreadcrumb", StringUtils.EMPTY);
        	String pageName = StringUtils.EMPTY;
			if(excludeProp.equalsIgnoreCase("true")) {
				pageName = currentPage.getName();
			}
			log.info("Exclude Property : {} with Page Name : {}", excludeProp, pageName);

            breadcrumbMap = new LinkedHashMap<>();
            Map<String, String> reverseMap = new LinkedHashMap<>();
            String requestUri = request.getRequestURI();
            if (requestUri.contains(HTML_EXTN) && !requestUri.contains(JcrConstants.JCR_CONTENT)) {
                final String currentPagePath = requestUri.split(HTML_EXTN)[0];
                final Resource currentRes = resourceResolver.getResource(currentPagePath);
                if (Objects.nonNull(currentRes)) {
                    Page page = currentRes.adaptTo(Page.class);
                    if (Objects.nonNull(page)) {
                        breadcrumbMap.put(page.getTitle(), currentPagePath.concat(HTML_EXTN));
                    }
                }
                createBreadcrumbList(currentPagePath);
                List<String> keySet = new ArrayList<>(breadcrumbMap.keySet());
                Collections.reverse(keySet);
                for (String key : keySet) {
                    reverseMap.put(key, breadcrumbMap.get(key));
                }
            }
        } catch (Exception e) {
            log.error("Exception in init() method of BreadcrumbModelImpl : {} ", e);
        }
    }

    /**
     * Method to create the Breadcrumb Map up-to Home Page
     *
     * @param currentPagePath
     * @return
     */
    private void createBreadcrumbList(String currentPagePath) {
        try {
            Resource resource = resourceResolver.getResource(currentPagePath);
            SlingQuery resources = $(resource).parents(CQ_PAGE);
            for (Resource res : resources) {
                if (Objects.nonNull(res)) {
                    Page page = res.adaptTo(Page.class);
                    if (Objects.nonNull(page)) {
                        breadcrumbMap.put(page.getTitle(), res.getPath().concat(HTML_EXTN));
                    }
                }
            }
        } catch (Exception e) {
            log.error("Error in BreadcrumbModelImpl : createBreadcrumbList() : ", e);
        }
    }
}
