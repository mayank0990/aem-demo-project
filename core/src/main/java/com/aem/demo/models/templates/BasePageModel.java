package com.aem.demo.models.templates;

import org.osgi.annotation.versioning.ConsumerType;

/**
 * Base Page Sling Model 
 *
 * @author Mayank
 *
 */
@ConsumerType
public interface BasePageModel {
	
	public String getTitle();
	
}
