package com.aem.demo.models;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.ws.rs.GET;

@Slf4j
@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class Sample {

    @Inject
    private String firstName;

    @Inject
    private String secondName;

    @PostConstruct
    protected void init() {
        log.debug("Inner Class init(...) method called...");
        firstName.toUpperCase();
    }

}
