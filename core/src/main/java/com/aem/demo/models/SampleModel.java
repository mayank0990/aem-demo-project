package com.aem.demo.models;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ResourcePath;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class SampleModel {

    @Inject
    private List<Resource> samples;

    private String title = "foo'bar";

    private String link = "https://foo'bar.html";

    private List<Sample> sampleList = new ArrayList<>();

    @PostConstruct
    protected void init() {
        /*log.debug("Super Class (SampleModel) init(...) method called...");
        if(!samples.isEmpty()) {
            samples.forEach(res -> {
                log.debug("Resource Path : {}", res.getPath());
                Sample obj = res.adaptTo(Sample.class);
                if(obj != null) {
                    sampleList.add(obj);
                }
            });
        }*/
    }
}