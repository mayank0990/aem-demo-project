package com.aem.demo.models;

import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Model(adaptables = {Resource.class}, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class SampleStudentsList {

	private static final String STUDENTS_DATA_PATH = "/etc/acs-commons/lists/demo-generic-list/jcr:content/list";
	
	@Inject
	private ResourceResolver resolver;
	
	@Inject
	private String heading;
	
	private List<Student> studentsData = new LinkedList<>();

	@PostConstruct
	public void init() {
		Resource studentsRes = resolver.getResource(STUDENTS_DATA_PATH);
		
		if(studentsRes != null) {
			Iterable<Resource> childIterable = studentsRes.getChildren();
			childIterable.forEach(studentObj -> {
				ValueMap prop = studentObj.getValueMap();
				Student student = new Student(prop.get("name", String.class), prop.get("subject", String.class), prop.get("address", String.class));
				
				studentsData.add(student);
			});			
		}
	}

	@Getter
	@NoArgsConstructor
	@AllArgsConstructor
	public class Student {		
		private String name;
		private String subject;
		private String address;
	}

}
