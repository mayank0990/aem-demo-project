package com.aem.demo.models.impl;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

/**
 * LinkField represents a link which contains label, URL and option to open the
 * link in new tab.
 * 
 * @author Mayank
 *
 */
@Model(adaptables = {Resource.class})
public class LinkField {

	@ValueMapValue
	@Default(values = StringUtils.EMPTY)
	private String linkLabel;

	@ValueMapValue
	@Default(values = StringUtils.EMPTY)
	private String linkUrl;

	@ValueMapValue
	@Default(values = StringUtils.EMPTY)
	private String newWindow;

	/**
	 * init() method of LinkField
	 */
	@PostConstruct
	protected void init() {

	}

	/**
	 * @return link label
	 */
	public String getLinkLabel() {
		return linkLabel;
	}

	/**
	 * @return link url
	 */
	public String getLinkUrl() {
		return linkUrl;
	}

	/**
	 * @return _blank when link will open in new tab, "" otherwise
	 */
	public String getNewWindow() {
		return newWindow;
	}

}
