package com.aem.demo.models;

import java.util.Calendar;

public interface SampleSlingModel {

	public String getTitle();

	public String getDescription(int truncateAt);

	public String getDescription();

	public long getSize();

	public Calendar getCreatedAt();

	public String getPath();

}
