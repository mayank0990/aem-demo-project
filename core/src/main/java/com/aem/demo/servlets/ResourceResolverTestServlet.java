package com.aem.demo.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(service = Servlet.class, property = {
		org.osgi.framework.Constants.SERVICE_DESCRIPTION + "= Resource Resolver Test Servlet",
		"sling.servlet.paths=/bin/resolver/test", "sling.servlet.methods=" + HttpConstants.METHOD_GET })
public class ResourceResolverTestServlet extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 7270861655022997150L;

	private static final Logger LOGGER = LoggerFactory.getLogger(ResourceResolverTestServlet.class);

	@Override
	protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
			throws ServletException, IOException {
		try {
			PrintWriter out = response.getWriter();
			ResourceResolver resolver = request.getResourceResolver();
			String path = request.getParameter("path");
			if(StringUtils.isNotBlank(path)) {
				Resource resource = resolver.getResource(path);
				String parentResTypeviaResource = resolver.getParentResourceType(resource);
				out.println("Parent Resource Type via Resource :: " + parentResTypeviaResource);
				
				
				String parentResType = resolver.getParentResourceType(path);
				out.println("Parent Resource Type via Path :: " + parentResType);
			}
		} catch(Exception e) {
			LOGGER.error("Error in doGet() method :: ResourceResolverTestServlet :: {}", e);
		}

	}

}
