package com.aem.demo.servlets;

import com.day.cq.i18n.I18n;
import lombok.extern.slf4j.Slf4j;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.service.component.annotations.Component;

import javax.annotation.Nonnull;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Locale;
import java.util.ResourceBundle;

import static org.osgi.framework.Constants.SERVICE_DESCRIPTION;

@Slf4j
@Component(service = Servlet.class, property = {
        SERVICE_DESCRIPTION + "= Sample Demo Servlet",
        "sling.servlet.paths=" + "/bin/sample/demo",
        "sling.servlet.methods=" + HttpConstants.METHOD_GET})
public class SampleServlet extends SlingSafeMethodsServlet {


    @Override
    protected void doGet(@Nonnull SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        out.println("Hello :D");
        Locale pageLang = new Locale("DE");
        ResourceBundle resourceBundle = request.getResourceBundle(pageLang);
        I18n i18n = new I18n(resourceBundle);
        String result = i18n.get("I had my breakfast...");
        out.println("Result : " + result);
    }
}
