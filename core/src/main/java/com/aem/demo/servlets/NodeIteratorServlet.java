package com.aem.demo.servlets;

import static org.apache.sling.query.SlingQuery.$;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.query.SlingQuery;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.demo.services.NodeIteratorService;

/**
 * @author Mayank
 *
 */
@Component(immediate = true, service = { Servlet.class }, property = {
		Constants.SERVICE_DESCRIPTION + "= Node Iterator Servlet", 
		"sling.servlet.methods=" + HttpConstants.METHOD_GET,
		"sling.servlet.paths=" + "/bin/node" })
public class NodeIteratorServlet extends SlingSafeMethodsServlet {

	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(NodeIteratorServlet.class);

	@Reference
	private NodeIteratorService nodeIteratorService;

	private List<Resource> listofResources;

	@Override
	protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
			throws IOException, ServletException {
		final PrintWriter out = response.getWriter();
		out.println("Node Iterator Servlet working...");
		LOGGER.info("Node Servlet Working Fine.");

		try {

			/*final ResourceResolver resourceResolver = request.getResourceResolver();
			final Resource resource = resourceResolver.getResource("/content/demo");
			final SlingQuery resources = $(resource).children();
			for (final Resource child : resources) {
				out.println("Path: " + child.getPath());
			}*/

			listofResources = nodeIteratorService.getResourceList(); 
			for (final Resource res : listofResources) { 
				out.println("Path: " + res.getPath()); 
			}

		} catch (final Exception e) {
			LOGGER.error("Error in NodeIteratorServlet : getResourceList() : ", e);
		}
	}
}