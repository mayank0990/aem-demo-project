package com.aem.demo.servlets;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.components.ComponentContext;
import com.day.cq.wcm.commons.WCMUtils;

@Component(immediate = true, service = { Servlet.class }, property = {
		Constants.SERVICE_DESCRIPTION + "= Component Context Servlet",
		"sling.servlet.methods=" + HttpConstants.METHOD_GET,
		"sling.servlet.paths=" + FetchResourceServlet.SERVLET_PATH })
public class CurrentComponentContext extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(FetchResourceServlet.class);

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {

		getCurrentPage(request);

	}

	private Page getCurrentPage(final Object adaptable) {
		if (adaptable instanceof SlingHttpServletRequest) {
			final ComponentContext context = getComponentContext(adaptable);
			if (context != null) {
				return context.getPage();
			}
		}
		return getResourcePage(adaptable);
	}

	/**
	 * Get the current component context.
	 * 
	 * @param adaptable a SlingHttpServletRequest
	 * @return the ComponentContext if the adaptable was a SlingHttpServletRequest,
	 *         or null otherwise
	 */
	private ComponentContext getComponentContext(Object adaptable) {
		if (adaptable instanceof SlingHttpServletRequest) {
			final SlingHttpServletRequest request = ((SlingHttpServletRequest) adaptable);
			return WCMUtils.getComponentContext(request);
		}
		// ComponentContext is not reachable from Resource
		return null;
	}

	private Page getResourcePage(final Object adaptable) {
		final PageManager pageManager = getPageManager(adaptable);
		final Resource resource = getResource(adaptable);
		if (pageManager != null && resource != null) {
			return pageManager.getContainingPage(resource);
		}
		return null;
	}

	private PageManager getPageManager(final Object adaptable) {
		final ResourceResolver resolver = getResourceResolver(adaptable);
		if (resolver != null) {
			return resolver.adaptTo(PageManager.class);
		}
		return null;
	}

	private ResourceResolver getResourceResolver(final Object adaptable) {
		if (adaptable instanceof SlingHttpServletRequest) {
			return ((SlingHttpServletRequest) adaptable).getResourceResolver();
		}
		if (adaptable instanceof ResourceResolver) {
			return (ResourceResolver) adaptable;
		}
		if (adaptable instanceof Resource) {
			return ((Resource) adaptable).getResourceResolver();
		}
		return null;
	}

	private Resource getResource(final Object adaptable) {
		if (adaptable instanceof SlingHttpServletRequest) {
			return ((SlingHttpServletRequest) adaptable).getResource();
		}
		if (adaptable instanceof Resource) {
			return (Resource) adaptable;
		}
		return null;
	}
}
