package com.aem.demo.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.demo.services.ModifyNodeService;

@Component(service = Servlet.class, property = {
		org.osgi.framework.Constants.SERVICE_DESCRIPTION + "= Simple Servlet",
		"sling.servlet.paths=/bin/simple", "sling.servlet.methods=" + HttpConstants.METHOD_GET })
@Designate(ocd = SampleConfigServlet.Config.class)
public class SampleConfigServlet extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 7270861655022997150L;

	private static final Logger LOGGER = LoggerFactory.getLogger(SampleConfigServlet.class);

	private boolean enableCaching;
	
	@Reference
	private ModifyNodeService modifyNodeService;

	@ObjectClassDefinition(name = "Sample Config Servlet ", description = "")
	@interface Config {
		@AttributeDefinition(name = "Cache Control", 
				description = "Time in seconds to cache response in the browser in the format max-age=<seconds>. Example: max-age=500",
				type = AttributeType.STRING)
		public String cacheControl();

		@AttributeDefinition(name = "Edge Control", 
				description = "Time to cache response in Akamai in the format max-age=<numeric-time><unit-of-time>. "
						+ "Unit of time can be h for hours, m for minutes and d for days. Example: max-age=8h for 8 hours.",
						type = AttributeType.STRING)
		public String edgeControl();

		@AttributeDefinition(name = "Enale Dispatcher Config", 
				description = "Enable caching the response in Dispatcher",
				type = AttributeType.BOOLEAN)
		public boolean enableCaching();
	}
	
	@Activate
	public void activate(final SampleConfigServlet.Config context) {
		enableCaching = context.enableCaching();
	}
	
	@Override
	protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
			throws ServletException, IOException {
		try {
			PrintWriter out = response.getWriter();			
			modifyNodeService.updateNode("/content/demo/jcr:content");
			out.println("Yay!");
			out.print("Enable Caching: " + enableCaching);
		} catch(Exception e) {
			LOGGER.error("Error in doGet() method :: SimpleServlet {} ", e);
		}
	}
}
