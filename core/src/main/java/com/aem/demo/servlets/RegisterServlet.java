package com.aem.demo.servlets;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.demo.services.MailSenderService;

@Component(service = Servlet.class, name = "User Register Servlet", property = {
		org.osgi.framework.Constants.SERVICE_DESCRIPTION + "= User Register Servlet",
		"sling.servlet.paths=/bin/register", "sling.servlet.methods=" + HttpConstants.METHOD_POST })
public class RegisterServlet extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 7270861655022997150L;

	private static final Logger LOGGER = LoggerFactory.getLogger(RegisterServlet.class);

	@Reference
	MailSenderService mailSenderService;

	@Override
	protected void doPost(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
			throws ServletException, IOException {

	}

}
