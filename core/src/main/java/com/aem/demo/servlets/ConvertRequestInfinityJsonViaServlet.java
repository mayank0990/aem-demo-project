package com.aem.demo.servlets;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.engine.SlingRequestProcessor;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.day.cq.contentsync.handler.util.RequestResponseFactory;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Mayank
 *
 */
@Slf4j
@Component(immediate = true, service= {Servlet.class},
property = { Constants.SERVICE_DESCRIPTION + "=" + "Convert Infinity JSON via Servlet", 
		"sling.servlet.methods=" + HttpConstants.METHOD_GET,
		"sling.servlet.paths="+ "/bin/get/infinity/json" })
public class ConvertRequestInfinityJsonViaServlet extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 1L;
	
	@Reference
	private transient RequestResponseFactory requestResponseFactory;

	@Reference
	private transient SlingRequestProcessor requestProcessor;

	@Activate
	protected void activate() {
		log.info("SampleServlet activated!");
	}

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {	
		try {
			PrintWriter out = response.getWriter();
			out.println("Convert Infinity JSON via Servlet running...\n");
			
			String contentPath = "/content/dam/indusind-corporate/investors/annual-reports/FY2018-2019/q1.infinity.json";			
			HttpServletRequest req = requestResponseFactory.createRequest("GET", contentPath);			
			ByteArrayOutputStream outStream = new ByteArrayOutputStream();
			HttpServletResponse resp = requestResponseFactory.createResponse(outStream);			
			requestProcessor.processRequest(req, resp, request.getResourceResolver());
			String jsonString = outStream.toString();
			out.println(jsonString);
			
			log.info("Json STRING :: {}", jsonString);
			
			out.close();
		} catch(Exception e) {
			log.error("Error in doGet() method :: SampleServlet :: {} ", e);
		}
	}	
}