package com.aem.demo.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Objects;

import javax.jcr.RepositoryException;
import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.jackrabbit.api.security.authentication.token.TokenCredentials;
import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.User;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.auth.core.spi.AuthenticationInfo;
import org.apache.sling.jcr.resource.api.JcrResourceConstants;
import org.apache.sling.settings.SlingSettingsService;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.crx.security.token.TokenCookie;

/**
 * 
 * Login Token Extractor Servlet
 * 
 * @author Mayank
 *
 */
@Component(immediate = true, service = { Servlet.class }, property = {
		Constants.SERVICE_DESCRIPTION + "= Login Token Extractor Servlet",
		"sling.servlet.methods=" + HttpConstants.METHOD_GET, "sling.servlet.paths=" + "/bin/extract" })
public class TokenExtractorServlet extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 7270861655022997150L;

	@Reference
	private transient SlingSettingsService settings;

	@Reference
	private transient ResourceResolverFactory resourceResolverFactory;

	private static final Logger LOGGER = LoggerFactory.getLogger(TokenExtractorServlet.class);

	@Override
	protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
			throws ServletException, IOException {
		try {
			PrintWriter out = response.getWriter();
			AuthenticationInfo authInfo = new AuthenticationInfo("TOKEN");
			TokenCredentials creds = null;
			TokenCookie.Info info = TokenCookie.getTokenInfo(request, "login");
			if (info != null && info.isValid()) {
				creds = new TokenCredentials(info.token);
			}
			authInfo.put(JcrResourceConstants.AUTHENTICATION_INFO_CREDENTIALS, creds);
			ResourceResolver resourceResolver = resourceResolverFactory.getResourceResolver(authInfo);
			if (!Objects.isNull(resourceResolver)) {
				User user = resourceResolver.adaptTo(User.class);
				if(Objects.nonNull(user)) {
					out.println("User ID via Token: " + user.getID());
					/* out.println("Remote USER: " + request.getRemoteUser()); */
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error in TokenExtractorServlet :: {}", e);
		}
	}

	/**
	 * Another way to create TokenCredentials Object
	 * 
	 * @param token
	 * @return
	 */
	/*
	 * private static TokenCredentials createCredentials(String token) { return new
	 * TokenCredentials(token); }
	 */

	/**
	 * Use the request to get the User who has (or will have)
	 * 
	 * @param request
	 * @return the User or null, if no User is associated with the request
	 */
	public User getCurrentUser(final SlingHttpServletRequest slingRequest) {
		try {
			final Authorizable authorizable = slingRequest.getResourceResolver().adaptTo(Authorizable.class);
			if (authorizable != null && !authorizable.isGroup() && !authorizable.getID().equals("anonymous")) {
				return (User) authorizable;
			}
		} catch (final RepositoryException e) {
			LOGGER.error("provider: disabled; failed identify user", e);
		}
		return null;
	}
}
