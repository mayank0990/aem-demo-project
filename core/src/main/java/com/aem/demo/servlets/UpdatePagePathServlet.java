package com.aem.demo.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.demo.utils.DITAUtils;
import com.day.commons.datasource.poolservice.DataSourceNotFoundException;
import com.day.commons.datasource.poolservice.DataSourcePool;

/**
 * Update Page Path Servlet in SQL
 * This API is used to update the PAGE_PATH Column of
 * which are having dita path. 
 * 
 * @author Mayank
 *
 */
@Component(service = Servlet.class, name = "Update Page Path Servlet", property = {
		org.osgi.framework.Constants.SERVICE_DESCRIPTION + "= Update Page Path Servlet",
		"sling.servlet.paths=" + UpdatePagePathServlet.SERVLET_PATH, "sling.servlet.methods=" + HttpConstants.METHOD_POST })
public class UpdatePagePathServlet extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 7270861655022997150L;

	private static final Logger LOGGER = LoggerFactory.getLogger(UpdatePagePathServlet.class);

	public static final String SERVLET_PATH = "/bin/update";
	
	private static final String DITA = "dita";
	private static final String DITAMAP = "ditamap";
	private static final String AEM_PATH = "AEM_PATH";
	private static final String SQL_DATASOURCE = "sqldatasource";
	
	String updateQuery = StringUtils.EMPTY;

	@Reference
	DataSourcePool dataSourcePool;

	PreparedStatement preparedStatement = null;

	@Activate
	protected void activate() {
		LOGGER.info("UpdatePagePathServlet Activated!");
	}

	@Override
	protected void doPost(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
			throws ServletException, IOException {
		try {
			PrintWriter out = response.getWriter();
			ResourceResolver resolver = request.getResourceResolver();
			boolean responseFlag = executeDb(resolver);
			if(responseFlag) {
				response.setStatus(HttpServletResponse.SC_OK);
				out.println("Updated Successfully.");
				response.flushBuffer();
			} else {
				response.setStatus(HttpServletResponse.SC_NOT_IMPLEMENTED);
				out.println("Not implemented!");
				response.flushBuffer();
			}
		} catch(Exception e) {
			LOGGER.error("Error in doGet() method of UpdatePagePathServlet : {}", e);
		}
	}

	public boolean executeDb(ResourceResolver resourceResolver) 
			throws SQLException, DataSourceNotFoundException, IOException {
		boolean responseFlag = false;
		final Connection connection = getConnection();
		try {
			if(!Objects.isNull(connection)) {
				final String selectQuery = "SELECT AEM_PATH from INFORM_REDIRECT WHERE AEM_PATH IS NOT NULL ORDER BY ID ASC";
				final ResultSet resultSet = selectQuery(connection, selectQuery);
				if(!Objects.isNull(resultSet)) {
					while(resultSet.next()) {
						String aemPath = resultSet.getString(AEM_PATH);
						String pagePath = DITAUtils.getPageFromXrefDita(aemPath, resourceResolver);
						if(StringUtils.isNotBlank(aemPath) && StringUtils.isNotBlank(pagePath) 
								&& !StringUtils.endsWith(pagePath, DITA) && !StringUtils.endsWith(pagePath, DITAMAP)) {
							updateQuery = "UPDATE INFORM_REDIRECT SET PAGE_PATH ='" + pagePath + "' WHERE AEM_PATH ='" + aemPath + "'"
									+ "and (FLAG IS NULL or FLAG = 'false')";
							int updateStatus = updatePagePathQuery(connection, updateQuery);
							LOGGER.info("Update Status: " + updateStatus + "\nFor Page Path: " + pagePath);
							responseFlag = true;
						}
					}
				}
			}			
		} catch(Exception e) {
			LOGGER.error("Exception in executeDb() method of UpdatePagePathServlet : {} ", e);
		} finally {
			if(!Objects.isNull(connection)) {
				connection.close();
			}
			if(!Objects.isNull(preparedStatement)) {
				preparedStatement.close();
			}
		}
		return responseFlag;
	}
	
	/**
	 * Method to execute Select Query
	 * 
	 * @param connection
	 * 
	 * @param selectQuery
	 * 
	 * @return
	 * 		{@link ResultSet}
	 */
	private ResultSet selectQuery(final Connection connection, final String selectQuery) {
		ResultSet resultSet = null;
		preparedStatement = null;
		try {
			LOGGER.info("Select Query: " + selectQuery);
			preparedStatement = connection.prepareStatement(selectQuery);
			resultSet = preparedStatement.executeQuery();
		} catch(Exception e) {
			LOGGER.error("Error in selectQuery method :: {}", e);
		}		
		return resultSet;
	}
	
	/**
	 * Method to execute Update Query
	 * 
	 * @param connection
	 * 
	 * @param selectQuery
	 * 
	 * @return
	 * 		{@link Integer}
	 */
	private int updatePagePathQuery(final Connection connection, final String updateQuery) {
		int updateStatus = 0;
		preparedStatement = null;
		try {
			LOGGER.info("Update Query: " + updateQuery);
			preparedStatement = connection.prepareStatement(updateQuery);
			updateStatus = preparedStatement.executeUpdate();
		} catch(Exception e) {
			LOGGER.error("Error in updateQuery method :: {}", e.getMessage());
		}
		return updateStatus;
	}

	/**
	 * Method to get Connection Object via Datasource
	 * 
	 * @return
	 * 		{@link Connection}
	 * 
	 * @throws SQLException
	 * 
	 * @throws DataSourceNotFoundException
	 */
	private Connection getConnection() throws SQLException, DataSourceNotFoundException {
		final DataSource dataSource = (DataSource) dataSourcePool.getDataSource(SQL_DATASOURCE);
		final Connection connection = dataSource.getConnection();
		return connection;
	}

}
