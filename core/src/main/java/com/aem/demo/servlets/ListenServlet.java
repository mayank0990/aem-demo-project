package com.aem.demo.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(immediate = true, service = { Servlet.class }, property = {
		Constants.SERVICE_DESCRIPTION + "= Listen Servlet", "sling.servlet.methods=" + HttpConstants.METHOD_GET,
		"sling.servlet.paths=" + ListenServlet.SERVLET_PATH })
public class ListenServlet extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(ListenServlet.class);

	public static final String SERVLET_PATH = "/bin/listen";

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		try {
			final PrintWriter out = response.getWriter();
			out.println("I am Called!");
		} catch (final Exception e) {
			LOGGER.error("Error in doGet :: ListenServlet : {}", e);
		}
	}

}
