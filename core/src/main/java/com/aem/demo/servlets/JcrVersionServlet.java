package com.aem.demo.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.jcr.Session;
import javax.jcr.version.Version;
import javax.jcr.version.VersionHistory;
import javax.jcr.version.VersionIterator;
import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;

@Component(service = Servlet.class, name = "JCR Version Servlet", property = {
		org.osgi.framework.Constants.SERVICE_DESCRIPTION + "= JCR Version Servlet",
		"sling.servlet.paths=/bin/version", "sling.servlet.methods=" + HttpConstants.METHOD_GET })
public class JcrVersionServlet extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 7270861655022997150L;

	private static final Logger LOGGER = LoggerFactory.getLogger(JcrVersionServlet.class);

	@Override
	protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
			throws ServletException, IOException {
		try {		
			PrintWriter out = response.getWriter();
			out.println("JcrVersion Servlet is working!");
			ResourceResolver resolver = request.getResourceResolver();
			Session session = resolver.adaptTo(Session.class);
			Resource pageResource = resolver.getResource("/content/pwc-madison/ditaroot/us/en/aicpa/ara-ebp_chapter_place_holder_2/ara-ebp_economic_and_industry_developments/ara-ebp_how_this_alert_helps");
			if(pageResource != null && pageResource instanceof Resource) {
				String jcrPath = StringUtils.EMPTY;
				Page page = pageResource.adaptTo(Page.class);
				if(page != null) {
					Resource jcrResource = page.getContentResource();
					if(jcrResource != null && jcrResource instanceof Resource) {
						jcrPath = jcrResource.getPath();
					}
				}
				VersionHistory history = session.getWorkspace()
						.getVersionManager()
						.getVersionHistory(jcrPath);
				VersionIterator versionIterator = history.getAllVersions();
				while(versionIterator.hasNext()) {
					Version version = (Version) versionIterator.next();
					out.println("version found : "+ version.getName() + " - " +
							version.getPath() + " - " + version.getCreated().getTime());
					out.println("version parent : " + version.getProperties());
				}
			}
		} catch(Exception e) {
			LOGGER.error("Error in doGet method :: JcrVersionServlet : {}", e.getMessage());
		}

	}

}
