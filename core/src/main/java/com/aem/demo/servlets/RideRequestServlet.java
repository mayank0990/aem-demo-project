package com.aem.demo.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Type;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;

import com.aem.demo.objects.Ride;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Component(service = Servlet.class, property = {
		org.osgi.framework.Constants.SERVICE_DESCRIPTION + "= Ride Request Parser Servlet",
		"sling.servlet.paths=/bin/ride/request", "sling.servlet.methods=" + HttpConstants.METHOD_GET })
public class RideRequestServlet extends  SlingAllMethodsServlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		Cookie rideCookie = request.getCookie("rideRequest");
		if(rideCookie != null) {
			String cookieVal = rideCookie.getValue();
			Type type = new TypeToken<Ride>() {}.getType();
			Gson gson = new Gson();
			Ride ride = gson.fromJson(cookieVal, type);
			out.println("Ride Object :: " + ride.toString());
		}
		out.close();
		response.flushBuffer();		
	}

}
