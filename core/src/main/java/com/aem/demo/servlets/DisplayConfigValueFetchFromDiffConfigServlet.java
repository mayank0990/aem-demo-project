package com.aem.demo.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.demo.services.ServiceToFetchProperties;

@Component(service = Servlet.class, immediate = true,
property = {

    "sling.servlet.methods=" + HttpConstants.METHOD_GET,
    "sling.servlet.paths=" + "/bin/fetchProp",
})
public class DisplayConfigValueFetchFromDiffConfigServlet extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 1L;
	
	/** Default Logger*/
	private static Logger LOGGER = LoggerFactory.getLogger(DisplayConfigValueFetchFromDiffConfigServlet.class);
	
	@Reference
	private transient ServiceToFetchProperties serviceConfig;

	@Override
	protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) 
			throws IOException, ServletException {
		try {
			String propVal = serviceConfig.fetchConfigProperty("com.aem.demo.services.impl.SampleServiceImpl", "name");
			PrintWriter out = response.getWriter();
			out.println("Prop Value " + propVal);
			out.close();			
		} catch(Exception e) {
			LOGGER.error("Error in doGet() :: {}", e);
		}
	}

}
