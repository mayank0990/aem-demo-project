package com.aem.demo.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.demo.services.UMSService;

@Component(
		immediate = true,
		service = { Servlet.class },
		enabled = true,
		property = {
				Constants.SERVICE_DESCRIPTION + "= UMS Servlet",
				"sling.servlet.methods=" + HttpConstants.METHOD_GET,
				"sling.servlet.paths=" + "/bin/ums"
		})
public class UMSServlet extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 1L;
	
	private static Logger LOGGER = LoggerFactory.getLogger(UMSServlet.class);
	
	ResourceResolver resourceResolver;
	
	@Reference
	UMSService umsService;
	
	@Activate
	protected void activate() {
		LOGGER.info("UMS Servlet Activated!");
	}
	
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException, ServletException {
		
		PrintWriter out = response.getWriter();
		out.println("UMS Servlet Working...");
		
		try {
			umsService.getUser();
			
		} catch(Exception e) {
			LOGGER.error("Exception in doGet method.. {} ", e);
		}	
	}
}
