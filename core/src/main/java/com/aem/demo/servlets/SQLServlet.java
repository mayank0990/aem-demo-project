package com.aem.demo.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.sql.DataSource;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceUtil;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.commons.datasource.poolservice.DataSourceNotFoundException;
import com.day.commons.datasource.poolservice.DataSourcePool;

@Component(
		immediate = true,
		service = { Servlet.class },
		enabled = true,
		property = {
				Constants.SERVICE_DESCRIPTION + "=SQL Connection Test Servlet",
				"sling.servlet.methods=" + HttpConstants.METHOD_GET,
				"sling.servlet.paths=" + "/bin/db"
		})
public class SQLServlet extends SlingSafeMethodsServlet {

	private static final long serialVersionUID = 1L;

	private static Logger log = LoggerFactory.getLogger(SQLServlet.class);

	@Reference
	DataSourcePool dataSourcePool;

	PreparedStatement preparedStatement = null;

	@Activate
	protected void activate() {
		log.info("SQLServlet activated!");

	}

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) 
			throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		out.println("SQL Servlet Servlet Working...");
		
		try {
			executeDb(response);
			
		} catch(Exception e) {
			log.info("SQL Exception in doGet ", e);
		}
		
	}

	public void executeDb(SlingHttpServletResponse response) throws SQLException, DataSourceNotFoundException, IOException {
		PrintWriter out = response.getWriter();
		out.println("executeDb Called!");
		
		final String query ="select madisonUrl from urlManipulation where informId = '0110031362640634'";
		final ArrayList<String> outputList = new ArrayList<String>();
		final Connection connection = getConnection();
		
		try {
			if(null != connection) {
				log.info("Got Connection!");
				preparedStatement = connection.prepareStatement(query);
				final ResultSet rs = preparedStatement.executeQuery();
				
				while(rs.next()) {
					outputList.add(rs.getString("madisonUrl"));
				}				
				out.println("Result: " + Arrays.toString(outputList.toArray()));
			}
			
		} catch(Exception e) {
			log.info("Exception in doGet ", e);

		} finally {
			if(null != connection) {
				connection.close();
			}
			if(null != preparedStatement) {
				preparedStatement.close();
			}
		}
	}

	private Connection getConnection() throws SQLException, DataSourceNotFoundException {
		final DataSource dataSource = (DataSource) dataSourcePool.getDataSource("sqldatasource");
		final Connection connection = dataSource.getConnection();
		return connection;
	}


}
