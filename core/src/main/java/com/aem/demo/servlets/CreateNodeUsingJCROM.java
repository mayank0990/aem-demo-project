/*
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *  ~ Copyright 2019 AEM Demo Project
 *  ~
 *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
package com.aem.demo.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.jcrom.Jcrom;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.aem.demo.objects.Employee;
import com.aem.demo.services.HttpUtilService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import lombok.extern.slf4j.Slf4j;

/**
 * Create Node Using JCR Object Mapper Servlet
 * 
 * @author Mayank
 *
 */
@Slf4j
@Component(immediate = true, service= {Servlet.class},
property = { Constants.SERVICE_DESCRIPTION + "=" + "Create JCR Node using JCR Object Mapper", 
		"sling.servlet.methods=" + HttpConstants.METHOD_GET,
		"sling.servlet.paths="+ "/bin/create/node" })
public class CreateNodeUsingJCROM extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 1L;
	
	@Reference
	private transient HttpUtilService httpUtilService;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) 
			throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		final ResourceResolver resolver = request.getResourceResolver();
		final Session session = resolver.adaptTo(Session.class);
		String json = httpUtilService.httpGet("http://localhost:3002/data");	// Add any Server/Microservice for Sample JSON Data
		Gson gson = new Gson();
		Type dataType = new TypeToken<List<Employee>> () {}.getType();
		List<Employee> employees = gson.fromJson(json, dataType);
		employees.forEach(employee -> out.println("Node Created : " + createNode(employee, session)));
	}

	/**
	 * Method createNode(...) To Create a Node in JCR
	 * 
	 * @param employee
	 * 
	 * @param session
	 * 
	 * @return
	 * 		{@link Boolean}
	 */
	public boolean createNode(final Employee employee, final Session session) {
		boolean status = false;
		try {			
			if(session != null) {
				Jcrom jcrrom = new Jcrom();
				Jcrom.setCurrentSession(session);
				jcrrom.map(Employee.class);			
				Node testNode = session.getNode("/content/testNode");
				jcrrom.addNode(testNode, employee);
				session.save();
				status = true;
			}
		} catch (RepositoryException e) {		
			log.error("Error occured in createNode(...) " + e.getMessage());
		}
		return status;
	}
}
