package com.aem.demo.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;
import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aem.demo.services.SampleFactoryService;

@Component(service = Servlet.class, property = {
		org.osgi.framework.Constants.SERVICE_DESCRIPTION + "= Simple Servlet for Factory Service",
		"sling.servlet.paths=/bin/serviceFactory", "sling.servlet.methods=" + HttpConstants.METHOD_GET })
public class SimpleServletForServiceFactory extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 7270861655022997150L;

	private static final Logger LOGGER = LoggerFactory.getLogger(SimpleServletForServiceFactory.class);

	@Nullable
	private transient List<SampleFactoryService> configList;
	
	/**
	 * Executed on Configuration Add event
	 * @param config New configuration for factory
	 */
	@Reference(name = "configurationFactory", cardinality = ReferenceCardinality.MULTIPLE,
			policy = ReferencePolicy.DYNAMIC)
	protected synchronized void bindConfigurationFactory(final SampleFactoryService config) {
		LOGGER.info("bindConfigurationFactory: {}", config.getName());
		if(configList == null) {
			configList = new ArrayList<>();
		}
		configList.add(config);
	}

	/**
	 * Executed on Configuration Remove event
	 * @param config New configuration for factory
	 */
	protected synchronized void unbindConfigurationFactory(final SampleFactoryService config) {
		LOGGER.info("unbindConfigurationFactory: {}", config.getName());
		configList.remove(config);
	}

	@Override
	protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response) 
			throws ServletException, IOException {
		try {
			PrintWriter out = response.getWriter();
			out.println("Calling Factory Service...");
			if(configList != null) {
				for(SampleFactoryService obj : configList) {
					out.println("New Config has been created! \nName: " + obj.getName());
				}
			}			
		} catch(Exception e) {
			LOGGER.error("Error in doGet() of SimpleServletForServiceFactory Servlet ::  {} ", e);
		}
	}


}
