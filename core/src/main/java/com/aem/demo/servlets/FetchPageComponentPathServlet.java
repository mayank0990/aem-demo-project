package com.aem.demo.servlets;

import static org.apache.sling.query.SlingQuery.$;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.query.SlingQuery;
import org.apache.sling.query.api.Predicate;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;

/**
 * This Servlet is used to fetch all the components list on the Page
 * 
 * @author Mayank
 *
 */
@Component(immediate = true, service = { Servlet.class }, property = {
		Constants.SERVICE_DESCRIPTION + "= Fetch Page Components Path Servlet",
		"sling.servlet.methods=" + HttpConstants.METHOD_GET,
		"sling.servlet.paths=" + FetchPageComponentPathServlet.SERVLET_PATH })
public class FetchPageComponentPathServlet extends SlingAllMethodsServlet {
	
	public static final String SERVLET_PATH = "/bin/fetchComponentsList";
	
	private static final long serialVersionUID = 1L;

	/** Default Logger */
	private static final Logger LOGGER = LoggerFactory.getLogger(FetchPageComponentPathServlet.class);

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		try {
			PrintWriter out = response.getWriter();
			ResourceResolver resolver = request.getResourceResolver();
			String pagePath = request.getParameter("pagePath");
			Resource resource = resolver.getResource(pagePath + "/jcr:content");
			if(resource instanceof Resource) {
				SlingQuery resources = $(resource).find(JcrConstants.NT_UNSTRUCTURED).filter(new Predicate<Resource>() {
					@Override
					public boolean accepts(Resource element) {
						String resourceType = element.getValueMap().get("sling:resourceType", StringUtils.EMPTY);
						LOGGER.info("Match: {}", resourceType.contains("aem-demo-project/components/content"));
						return resourceType.contains("aem-demo-project/components/content");
					}	
				});
				for(Resource res : resources) {
					out.println("Component Path: " + res.getResourceType());
				}
			}
		} catch(Exception e) {
			LOGGER.error("Error in doGet() method :: {}", e);
		}
	}
}
