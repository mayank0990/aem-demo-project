package com.aem.demo.servlets;

import javax.servlet.Servlet;

import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(
		immediate = true,
		service = { Servlet.class },
		enabled = true,
		property = {
				Constants.SERVICE_DESCRIPTION + "=Madison Redirect URL POC Servlet",
				"sling.servlet.methods=" + HttpConstants.METHOD_GET,
				"sling.servlet.paths=" + "/bin/madison"
		})
public class MadisonRedirectServlet extends SlingSafeMethodsServlet {

	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(MadisonRedirectServlet.class);

	/*Mongo Fields*/
	/*private MongoClient mongoClient;
	private MongoDatabase database;
	private MongoCollection<Document> collection;*/

	private String url = "";
	private String informId = "";
	private String tempId = "";
	private String madisonUrl = "";
	private String regex = "\\d+";

	@Activate
	protected void activate() {
		log.info("Madison Redirect URL Servlet activated!");

	}

	/*@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) 
			throws ServletException, IOException {
		//PrintWriter out = response.getWriter();
		//String completeUrl = request.getRequestURL()+"?"+request.getQueryString();
		this.url = request.getQueryString().split("url=")[1];
		//out.println("URL QUery Param " + url);
				
		try {		
			this.mongoClient = new MongoClient("localhost", 27017);
			this.database = mongoClient.getDatabase("madison");
			this.collection = database.getCollection("redirect");
			log.info("Database - " + database.getName() 
					+ " :: Connect Point - " 
					+ mongoClient.getConnectPoint() 
					+ " Up & Running");

			if(url.contains("id")) {
				// URL Containing ID Parameter
				tempId = url.split("id=")[1];			
				if(!tempId.matches(regex)) {
					informId = tempId.replaceAll("[^0-9]", "");
					madisonUrl = redirectUrl(informId);
					if(madisonUrl != "") {
						response.setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY);
						response.setHeader("Location", madisonUrl);
					} else {
						response.setStatus(HttpServletResponse.SC_NOT_FOUND);
					}
				} else {
					madisonUrl = redirectUrl(tempId);
					if(madisonUrl != "") {
						response.setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY);
						response.setHeader("Location", madisonUrl);
					} else {
						response.setStatus(HttpServletResponse.SC_NOT_FOUND);
					}
				}
			} else {
				// URL Not containing ID Parameter
				String[] urlParts = url.split("/");
				informId = urlParts[urlParts.length-1];
				madisonUrl = redirectUrl(informId);
				if(madisonUrl != "") {
					response.setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY);
					response.setHeader("Location", madisonUrl);
				} else {
					response.setStatus(HttpServletResponse.SC_NOT_FOUND);
				}
			}
		} catch(Exception e) {
			log.debug("Exception in MadisonRedirectServlet :: " + e.getMessage());
		}
		finally {
			mongoClient.close();
			log.info("Connection closed!");
		}
	}
	
	public String redirectUrl(String informId) {
		String url = "";
		try {
			Bson filter = Filters.eq("informId", informId);
			for(Document doc : collection.find(filter).limit(1)) {
				url = doc.getString("madisonUrl");
			}
		} catch(Exception e) {
			log.debug("Exception in redirectUrl ", e.getMessage());
		}
		
		return url != "" ? url : "";
	}*/
}
