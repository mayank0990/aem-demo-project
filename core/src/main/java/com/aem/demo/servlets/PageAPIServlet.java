package com.aem.demo.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.inject.Inject;
import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.export.json.SlingModelFilter;
import com.aem.demo.utils.DITAUtils;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.Template;

@Component(service = Servlet.class, name = "Page API Servlet", property = {
		org.osgi.framework.Constants.SERVICE_DESCRIPTION + "= Page API Servlet",
		"sling.servlet.paths=/bin/pageApi", "sling.servlet.methods=" + HttpConstants.METHOD_GET })
public class PageAPIServlet extends  SlingAllMethodsServlet {

	private static final long serialVersionUID = 7270861655022997150L;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PageAPIServlet.class);
	
	@Inject
    private SlingModelFilter slingModelFilter;

	@Override
	protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
			throws ServletException, IOException {
		try {
			PrintWriter out = response.getWriter();
			String path = "/content/demo/demo1";
			ResourceResolver resourceResolver = request.getResourceResolver();
			Resource pageResource = resourceResolver.getResource(path);
			
			String pagePath = DITAUtils.getPageFromXrefDita("/content/dam/pwc-madison/ditaroot/us/en/aicpa/aicpa_1/ara-ebp_notice_to_readers.dita", resourceResolver);
			out.println("Page Path from Xref DITA : " + pagePath);
			if(pageResource != null && pageResource instanceof Resource) {
				Page page = pageResource.adaptTo(Page.class);
				if(page != null && page instanceof Page) {
					Resource contentResource = page.getContentResource();	// To get JCR Content of the Page
					out.println("Content Resource: " + contentResource.getPath());
					
					Template template = page.getTemplate();
					out.println("Page Template: " + template.getPath());
				}
			}
			
		} catch(Exception e) {
			LOGGER.error("Error in doPost method :: PageAPIServlet {} ", e);
		}

	} 
}
