package com.aem.demo.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.inject.Inject;
import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;

import com.aem.demo.objects.Student;
import com.aem.demo.services.RequestParserService;
import com.google.gson.Gson;

@Component(service = Servlet.class, name = "Request Parser Servlet", property = {
		org.osgi.framework.Constants.SERVICE_DESCRIPTION + "= Request Parser Servlet",
		"sling.servlet.paths=/bin/request/data", "sling.servlet.methods=" + HttpConstants.METHOD_POST })
public class RequestParserServlet extends  SlingAllMethodsServlet {

	private static final long serialVersionUID = 7270861655022997150L;
	
	@Inject
    private RequestParserService requestParserService;

	@Override
	protected void doPost(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
			throws ServletException, IOException {
		Student student = (Student) requestParserService.getMappedObject(request, Student.class);
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		out.println(new Gson().toJson(student));
		out.close();
		response.flushBuffer();		
	}

}
