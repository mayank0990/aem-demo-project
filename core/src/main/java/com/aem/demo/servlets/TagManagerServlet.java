package com.aem.demo.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.jcr.api.SlingRepository;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.RangeIterator;
import com.day.cq.tagging.TagManager;

@Component(
		immediate = true,
		service = { Servlet.class },
		enabled = true,
		property = {
				Constants.SERVICE_DESCRIPTION + "= Tag Manager Servlet Config",
				"sling.servlet.methods=" + HttpConstants.METHOD_GET,
				"sling.servlet.paths=" + "/bin/tag"
		})
public class TagManagerServlet extends SlingSafeMethodsServlet {

	private static final long serialVersionUID = 1L;
	
	/** Default Log*/
	protected final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
	
	@Reference
	private SlingRepository repository;
	
	private ResourceResolver resourceResolver;
	
	@Activate
	protected void activate() {
		LOGGER.info("TagManagerServlet Activated!");
	}
	
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		out.println("Tag Manager Servlet Working...");
						
		try {
			resourceResolver = request.getResourceResolver();
			
			TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
			String myTag = "colors:Red";
			
			RangeIterator<Resource> tagsIt = tagManager.find(myTag);
			
			String resPath = StringUtils.EMPTY;
			List<String> list = new ArrayList<String>();
			int count = 1;
			
			while(tagsIt.hasNext()) {
				Resource res = (Resource) tagsIt.next();
				resPath = res.getPath();
				list.add(resPath);
				out.println("Resources Path :: " + resPath + " :: " + res.getParent().getName() + " :: " + count++);
			}
			
		} catch(Exception e) {
			LOGGER.error("Error in TagManagerServlet doGet() : {} ", e);
		}
	}
	
	public String getRepositoryName() {
		return repository.getDefaultWorkspace();
	}
	

}
