package com.aem.demo.servlets;

import static org.apache.sling.query.SlingQuery.$;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.query.SlingQuery;
import org.apache.sling.query.api.Predicate;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.wcm.api.Page;

@Component(immediate = true, service = { Servlet.class }, property = {
		Constants.SERVICE_DESCRIPTION + "= Fetch Resource Servlet", "sling.servlet.methods=" + HttpConstants.METHOD_GET,
		"sling.servlet.paths=" + FetchResourceServlet.SERVLET_PATH })
public class FetchResourceServlet extends SlingAllMethodsServlet {

	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(FetchResourceServlet.class);

	public static final String SERVLET_PATH = "/bin/fetch";

	private static final String CQ_PAGE = "cq:Page";
	private static final String PAGE_PATH = "/content/demo";
	private static final String PAGE_PROP = "Invincible";
	private static final String NODE_NAME = "demo";
	private static final String NEWS_DETAIL_TEXT = "newsDetailText";

	private Resource pageFound;
	private Resource compFound;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		try {
			final PrintWriter out = response.getWriter();
			out.println("Fetch Resource Servlet Working...");

			final ResourceResolver resourceResolver = request.getResourceResolver();
			final Resource pageRes = resourceResolver.getResource(PAGE_PATH); // Parent Page Resource

			if (pageRes != null & pageRes instanceof Resource) {
				final SlingQuery pageResource = $(pageRes).find(CQ_PAGE).filter(new Predicate<Resource>() {
					@Override
					public boolean accepts(Resource element) {
						return element.adaptTo(Page.class).getProperties().containsValue(PAGE_PROP);
					}
				});

				if (!pageResource.asList().isEmpty()) {
					pageFound = pageResource.asList().get(0);
					out.println("Page Path: " + pageFound.getPath()); // Page containing the unique pageId

					if (pageFound != null & pageFound instanceof Resource) {
						final SlingQuery nodeResource = $(pageFound).find(JcrConstants.NT_UNSTRUCTURED)
								.filter(new Predicate<Resource>() {
									@Override
									public boolean accepts(Resource element) {
										return element.getName().equalsIgnoreCase(NODE_NAME);
									}
								});
						if (!nodeResource.asList().isEmpty()) {
							compFound = nodeResource.asList().get(0);
							out.println("Comp Path: " + compFound.getPath()); // newsDetail Component

							if (compFound != null & compFound instanceof Resource) {
								final ValueMap compProps = compFound.getValueMap();
								out.println("Title: " + compProps.get("newsTitle", StringUtils.EMPTY));

								final Resource newsDetailText = compFound.getChild(NEWS_DETAIL_TEXT);
								if (newsDetailText != null & newsDetailText instanceof Resource) {
									out.println("News Detail Text: " + newsDetailText.getPath()); // newsDetailText
																									// Inner Component
									// ValueMap detailProps = newsDetailText.getValueMap();
									// Fetch any property from the ValueMap.
								}
							}
						}
					}
				}
			}
		} catch (final Exception e) {
			LOGGER.info("Error in doGet() FetchResourceServlet: {} ", e);
		}
	}

}
