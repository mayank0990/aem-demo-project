package com.aem.demo.objects;

import org.jcrom.annotations.JcrName;
import org.jcrom.annotations.JcrPath;
import org.jcrom.annotations.JcrProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Employee {
	
	@JcrName private String nodeName;
	@JcrProperty private String employeeId;
	@JcrProperty private String employeeDept;
	@JcrProperty private String employeeName;
	@JcrPath private String path;
	
}
