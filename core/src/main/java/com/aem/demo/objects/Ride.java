package com.aem.demo.objects;

import java.util.List;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class Ride {

	private String startPoint;
	private List<Double> startPointCoordinates;
	private String endPoint;
	private List<Double> endPointCoordinates;
	private String createdBy;
	private String rideName;
	private String difficulty;
	private String startDate;
	private String endDate;
	private String durationInDays;
	private String totalDistance;
	private String rideDetails;
	private String terrainType;
	private String startTime;
	private String rideStatus;
	private List<String> rideCategory;
	private String rideType;
	private List<Waypoints> waypoints;
}
