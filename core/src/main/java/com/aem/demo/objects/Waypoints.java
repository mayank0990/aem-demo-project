package com.aem.demo.objects;

import lombok.Getter;

@Getter
public class Waypoints {
	
	private String latitude;
	private String longitude;
}
