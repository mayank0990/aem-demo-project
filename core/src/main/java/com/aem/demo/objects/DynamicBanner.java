package com.aem.demo.objects;

public class DynamicBanner {
	
	private String entity;
	private String imagePath;
	
	public String getEntity() {
		return entity;
	}
	
	public void setEntity(String entity) {
		this.entity = entity;
	}
	
	public String getImagePath() {
		return imagePath;
	}
	
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	
	@Override
	public String toString() {
		return "DynamicBanner [entity=" + entity + ", imagePath=" + imagePath + "]";
	}

}
