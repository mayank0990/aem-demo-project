package com.aem.demo.workflows;

import java.util.List;

import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.HistoryItem;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.WorkflowData;
import com.adobe.granite.workflow.exec.WorkflowProcess;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.adobe.granite.workflow.model.WorkflowNode;


@Component(service = WorkflowProcess.class, property = {
		"process.label=" + "Good Process"
})
public class ApproveStep implements WorkflowProcess {

	protected final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	// Inject a Sling ResourceResolverFactory
	@Reference
	private ResourceResolverFactory resolverFactory;
	
	// Inject NodeUpdateProperty Service

	@Override
	public void execute(WorkItem item, WorkflowSession session, MetaDataMap args) throws WorkflowException {

		try {
			LOGGER.info("**** Here in execute Method");

			// Get the Assets from the File System for a Test
			WorkflowNode myNode = item.getNode();

			String myTitle = myNode.getTitle();	// Returns the Title of the Workflow Step		
			LOGGER.info("----- The Title is: " + myTitle);

			WorkflowData workflowData = item.getWorkflowData();	// Gain access to the payload Data
			String path = workflowData.getPayload().toString();	// Get the path of the Asset

			// Get only the name of asset - including the extension
			int index = path.lastIndexOf("/");
			String fileName = path.substring(index + 1);

			String approvedUser = getUserWhomApproved(session, item);

			// Get the User Whom Approved the workflow
			LOGGER.info("**** This asset was acceptes " + fileName + " and approved by  " + approvedUser);

			// Call the Service method to Update the Node Property whether the asset has been approved or not. 
			

		} catch(Exception e) {
			LOGGER.error("Error in execute() :: ApproveStep :: {} ", e);
			e.printStackTrace();
		}
	}

	// Get the User who approves the Payload
	private String getUserWhomApproved(WorkflowSession session, WorkItem item) {
		try {
			List<HistoryItem> historyList = session.getHistory(item.getWorkflow());
			int listSize = historyList.size();	// Get the Size of the List			
			HistoryItem lastItem = historyList.get(listSize - 1);
			String lastComment = lastItem.getComment();
			String lastAction = lastItem.getAction();
			String lastUser = lastItem.getUserId();
			return lastUser;			
		} catch(Exception e) {
			LOGGER.error("Error in method getUserWhomApproved :: {} ", e);
			e.printStackTrace();
		}
		return "error - No User";
	}

}
