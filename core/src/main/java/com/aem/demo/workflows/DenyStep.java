package com.aem.demo.workflows;

import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.WorkflowData;
import com.adobe.granite.workflow.exec.WorkflowProcess;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.adobe.granite.workflow.model.WorkflowNode;

@Component(service = WorkflowProcess.class, property = {
		"process.label=" + "Deny Process"
})
public class DenyStep implements WorkflowProcess {

	/** Default Logger */
	protected final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
	
	// Inject a Sling ResourceResolverFactory
	@Reference
	private ResourceResolverFactory resolverFactory;
	
	@Override
	public void execute(WorkItem item, WorkflowSession session, MetaDataMap args) throws WorkflowException {
		try {
			LOGGER.info("***** Here is exeucte method of DenyStep");
			
			// Get the Assets from the file system for a test
			WorkflowNode myNode = item.getNode();
			
			String nodeTitle = myNode.getTitle();	// returns the title of the workflow step
			LOGGER.info("*** The title is: " + nodeTitle);
			
			WorkflowData workflowData = item.getWorkflowData();	// Gain access to the payload data
			String path = workflowData.getPayload().toString();	// Get the Path of the Asset
			
			// Get only the name of the Asset - Including the extension
			int index = path.lastIndexOf("/");
			String fileName = path.substring(index + 1);
			
			LOGGER.info("**** The Asset was rejected " + fileName);
			
		} catch(Exception e) {
			LOGGER.error("Error in execute() method of DenyStep :: {} ", e);
			e.printStackTrace();
		}		
	}

}
