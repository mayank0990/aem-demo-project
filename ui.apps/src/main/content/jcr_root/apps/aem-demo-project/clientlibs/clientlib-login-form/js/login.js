$(function() {
	
	/**
	 * Login Click
	 */
    $("#signin").on("click", function(e) {		
        let username = $("#username").val();
        let password = $("#pass").val();
        let data = {
            username: username,
            pass: password
        };
        if(username != "" && password != "") {
            $.ajax({
                url : '/customAuthHandler',
                type: "POST",
                data: data,
                success: function(res) {
                    console.log(res);
                    window.location = "https://www.google.com";
                },
                error: function(e) {
                  console.log(e);
                }
              });
             e.preventDefault();
        } else {
            if(username) {
                alert("Please Enter the Password");
            } else if(password) {
                alert("Please Enter the Username");
            } else {
				alert("Please don't leave the fields blank!");
            }
            e.preventDefault();
        }
    });
    
    /**
     * Register Click
     */
    $("#signup").on("click", function(e) {
    	let firstName = $("#firstName").val();
    	let lastName = $("#lastName").val();
    	let email = $("#email").val();
    	let password = $("#pass").val();
    	let rePassword = $("#re_pass").val();
    	
    	let data = {
			firstName : firstName,
			lastName : lastName,
			email : email,
			password : password
    	};    	
    	if(firstName != "" && lastName != "" && email != "" && password != "") {
    		$.ajax({
        		url : "/bin/user/reg",
        		type: "POST",
        		contentType: "application/json",
        		data : JSON.stringify(data),
        		success: function(res) {
        			console.log(res);
        			alert("Congratulations! You've been successfully registered!");
        		},
        		error: function(er) {
        			console.log(er);
        		}
        	});  
    		e.preventDefault();
    	} else {
    		alert("All the fields are necessary!");
    		e.preventDefault();
    	}
    	
    });
    
});