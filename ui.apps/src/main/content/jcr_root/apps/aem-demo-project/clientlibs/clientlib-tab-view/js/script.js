(function ($, $document) {
    "use strict";    
    var mode, defaultMode, manualBtnHide, manualBtnShow, tableShow, tableHide, 
        submitBtnHide, submitBtnShow, activity, date;
    
    $(document).on("dialog-ready", function() {
		//console.log("DIALOG READY");
    	
        /**
         * Initializing Variables and functions on Load time (Dialog-Ready Event)
		 */        
        defaultMode = $("#custom-tabs").val();        
        manualBtnHide = function() {
            $("#manual-btn").hide();
        }
        manualBtnShow = function() {
			$("#manual-btn").show();
        }
        tableShow = function() {
			$("#manual-table").show();
        }
        tableHide = function() {
			$("#manual-table").hide();
        }
        submitBtnHide = function() {
			$("#submit-btn").hide();
        }
        submitBtnShow = function() {
            $("#submit-btn").show();
        }

        if(defaultMode === "auto") {
			manualBtnHide();
            tableHide();
            submitBtnHide();
        }

        /**
         *	Change Method for the Mode Dropdown
         */
        $('#custom-tabs').on('change', function(event) {
          	mode = $(this).val();
            //console.log("Mode Changed to: ", mode);
            if(mode === "manual") {
                manualBtnShow();
            } else {
				manualBtnHide();
                tableHide();
                submitBtnHide();
            }
        });

        /**
         *	Click method for Preview Button
         */
        $("#manual-btn").on('click', function(event) {
			tableShow();
            submitBtnShow();
        });

        /**
         *	Click method for Submit Button to Display the Selected Items in the Table
         */
        $("#submit-btn").on('click', function(event) {
            $('#manual-table-body tr:selected').each(function() {
                activity = $(this).find(".activity").html(); 
                date = $(this).find(".date").html();
                console.log(`Activity: ${activity}, Date: ${date}`);
             });
        });
    });
})($, $(document));